'use strict';

/**
 * Module dependencies.
 */
var config = require('../config'),
  chalk = require('chalk'),
  path = require('path'),
  mongoose = require('mongoose');

// Load the mongoose models
module.exports.loadModels = function (callback) {
  // Globbing model files
  config.files.server.models.forEach(function (modelPath) {
    require(path.resolve(modelPath));
  });

  if (callback) callback();
};

// Initialize Mongoose
module.exports.connect = function (cb) {
  var _this = this;

  var db = mongoose.connect(config.db.uri, config.db.options, function (err) {
    // Log Error
    if (err) {
      console.error(chalk.red('Could not connect to MongoDB!'));
      console.log(err);
    } else {


    //var Product = mongoose.model('Product');
  
    //Product.find({})
    //   .then(function() {
        //console.log('populating products');
        // Product.create(
    
        //     {
        //         name: 'Rorscharch Red',
        //         description: 'Some t is a long established fact that a reader will be distracted by the is a long established fact that a reader will be distracted by the ',
        //         info: 'Some info here',
        //         active: true,
        //         category:'women',//MEN,WOMAN
        //         subcategory:'rorscharch',//yachting,Signature,Alligator,Rorsarch
        //         price:{ value:'190', currency:'£' },
        //         stock:10,
        //         image:'/modules/core/client/img/assets/designs/rorscharch/red.jpg'
        //     },
        //     {
        //         name: 'Rorscharch Blue',
        //         description: 'Some t is a long established fact that a reader will be distracted by the is a long established fact that a reader will be distracted by the ',
        //         info: 'Some info here',
        //         active: true,
        //         category:'women',//MEN,WOMAN
        //         subcategory:'rorscharch',//yachting,Signature,Alligator,Rorsarch
        //         price:{ value:'190',currency:'£' },
        //         stock:0,
        //         image:'/modules/core/client/img/assets/designs/rorscharch/blue.jpg'
        //     },
        //     {
        //         name: 'Alligator Black',
        //         description: 'Some t is a long established fact that a reader will be distracted by the is a long established fact that a reader will be distracted by the ',
        //         info: 'Some info here',
        //         active: true,
        //         category:'men',//MEN,WOMAN
        //         subcategory:'alligator',//yachting,Signature,Alligator,Rorsarch
        //         price:{ value:'150',currency:'£' },
        //         stock:17,
        //         image:'/modules/core/client/img/assets/alligators/alligatorBlack.jpeg'
        //     },
        //     {
        //         name: 'Alligator Brown',
        //         description: 'Some t is a long established fact that a reader will be distracted by the is a long established fact that a reader will be distracted by the ',
        //         info:  'Some info here',
        //         active: true,
        //         category:'men',//MEN,WOMAN
        //         subcategory:'alligator',//yachting,Signature,Alligator,Rorsarch
        //         price:{ value:'150',currency:'£' },
        //         stock:0,
        //         image:'/modules/core/client/img/assets/alligators/alligatorBrown.jpg'

        //     },
        //     {
        //         name: 'Alligator Olive',
        //         description: 'Some t is a long established fact that a reader will be distracted by the is a long established fact that a reader will be distracted by the ',
        //         info:  'Some info here',
        //         active: true,
        //         category:'men',//MEN,WOMAN
        //         subcategory:'alligator',//yachting,Signature,Alligator,Rorsarch
        //         price:{ value:'150',currency:'£' },
        //         stock:20,
        //         image:'/modules/core/client/img/assets/alligators/alligatorOlive.jpg'

        //     },
    
        //     {
        //         name: 'Yachting Color',
        //         description: 'Some t is a long established fact that a reader will be distracted by the is a long established fact that a reader will be distracted by the ',
        //         info:  'Some info here',
        //         active: true,
        //         category:'men',//MEN,WOMAN
        //         subcategory:'yatching',//yachting,Signature,Alligator,Rorsarch
        //         price:{ value:'150',currency:'£' },
        //         stock:20,
        //         image:'/modules/core/client/img/assets/yatching/yatchColorScarf.jpg'

        //     },
        //     {
        //         name: 'Yachting B&W',
        //         description: 'Some t is a long established fact that a reader will be distracted by the is a long established fact that a reader will be distracted by the ',
        //         info:  'Some info here',
        //         active: true,
        //         category:'men',//MEN,WOMAN
        //         subcategory:'yatching',//yachting,Signature,Alligator,Rorsarch
        //         price:{ value:'150',currency:'£' },
        //         stock:5,
        //         image:'/modules/core/client/img/assets/yatching/yatch-black-white-scarf.jpg'


        //     },
    
        //     {
        //         name: 'Fingerprint Stone',
        //         description: 'Some t is a long established fact that a reader will be distracted by the is a long established fact that a reader will be distracted by the ',
        //         info:  'Some info here',
        //         active: true,
        //         category:'men',//MEN,WOMAN
        //         subcategory:'fingerprint',//yachting,Signature,Alligator,Rorsarch
        //         price:{ value:'150',currency:'£' },
        //         stock:0,
        //         image:'/modules/core/client/img/assets/fingerprints/fingerprintStone.jpg'

        //     },

        //     {
        //         name: 'Fingerprint Grey',
        //         description: 'Some t is a long established fact that a reader will be distracted by the is a long established fact that a reader will be distracted by the ',
        //         info:  'Some info here',
        //         active: true,
        //         category:'men',//MEN,WOMAN
        //         subcategory:'fingerprint',//yachting,Signature,Alligator,Rorsarch
        //         price:{ value:'150',currency:'£' },
        //         stock:10,
        //         image:'/modules/core/client/img/assets/fingerprints/fingerprintGrey.jpg'
        //     }
        // );
    //});

    // Enabling mongoose debug mode if required
    mongoose.set('debug', config.db.debug);

    // Call callback FN
    if (cb) cb(db);
    }
  });
};

module.exports.disconnect = function (cb) {
  mongoose.disconnect(function (err) {
    console.info(chalk.yellow('Disconnected from MongoDB.'));
    cb(err);
  });
};
