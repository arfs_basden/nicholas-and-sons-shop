'use strict';

module.exports = {
  client: {
    lib: {
      css: [
        'public/lib/bootstrap/dist/css/bootstrap.min.css',
        'public/lib/angular-toastr/dist/angular-toastr.css',
        'public/lib/font-awesome/css/font-awesome.min.css'
      ],
      js: [
        'public/lib/jquery/dist/jquery.min.js',
        'public/lib/angular/angular.min.js',
        'public/lib/angular-resource/angular-resource.min.js',
        'public/lib/angular-animate/angular-animate.min.js',
        'public/lib/angular-messages/angular-messages.min.js',
        'public/lib/angular-cookies/angular-cookies.min.js',
        'public/lib/angular-toastr/dist/angular-toastr.min.js',
        'public/lib/angular-toastr/dist/angular-toastr.tpls.min.js',
        'public/lib/angular-ui-router/release/angular-ui-router.min.js',
        'public/lib/angular-ui-utils/ui-utils.min.js',
        'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
        'public/lib/angular-file-upload/angular-file-upload.min.js',
        'public/lib/spin.js/spin.min.js',
        'public/lib/angular-spinner/angular-spinner.min.js',
        'public/lib/angular-credit-cards/release/angular-credit-cards.js',
        'public/lib/angular-stripe/release/angular-stripe.js',
        'public/lib/angular-payments/lib/angular-payments.min.js',
        'public/lib/owasp-password-strength-test/owasp-password-strength-test.js'
      ]
    },
    css: 'public/dist/application.min.css',
    js: 'public/dist/application.min.js'
  }
};
