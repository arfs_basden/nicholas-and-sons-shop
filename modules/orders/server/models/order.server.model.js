'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Order Schema
 */
var OrderSchema = new Schema({
  name: {
    type: String,
    default: '',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  cart: {
      type: Schema.ObjectId,
      ref: 'Cart'
  },
  status: {
    name:{
      type: String,
      default: 'IN PROCESS'//IN PROCESS, IN DELIVERY, COMPLETE
    },
    description:{
       type: String,
    }
  },
  stripe:{},
  client: {
    name:{
      type: String,
    },
    id:{
      type: String,
    },
    ip:{
      type: String,
    },
    token:{
      type: String,
    }
  },

  shippingAddress:[
    {
      addressLine1:{
        type: String
      },
      addressLine2:{
        type: String
      },
      city:{
        type: String
      },
      country:{
        type: String,
        default: 'United Kingdom'
      },
      postalCode:{
        type: String
      },
      region:{
        type: String
      },
  }]

});

mongoose.model('Order', OrderSchema);
