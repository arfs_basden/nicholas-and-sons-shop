'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Order = mongoose.model('Order'),
  Cart = mongoose.model('Cart'),
  User = mongoose.model('User'),
  async = require('async'), 
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

  var stripe = require('stripe')('sk_test_a8BnWcuis9tgZN3WO0sj2Jbd');

/**
* Create a Order
*/
exports.create = function(req, res) {
  
    var order = new Order();
    var shoppingCart = new Cart();
    var user = req.user;
    var stripeToken = req.body.token;

  
    async.waterfall([
        function(callback){
            //find cart and deactivate
            Cart.findById(user.cart).populate('products.product').exec(function (err, cart) {
                if (err) {
                    callback(err);
                } else if (!cart) {
                    callback(err);
                }

                //deactivate the cart
                cart.status='not-active';
                cart.save(function(err) {
                  if (err) {
                    callback(err);
                  } else {
                    callback(null, cart);
                  }
                });
            });
        },
        function(currentCart,callback){
        // amount in cents
        var charge=Number(currentCart.totalPrice)*100;    
        var customerId=user.stripeCustomerId;
            if(customerId!==undefined){
                stripe.customers.retrieve(user.stripeCustomerId)
                    .then(function(customer){
                        return stripe.charges.create({
                            amount: charge, // amount in cents, again
                            currency: 'gbp',
                            customer: customer.id,
                            description:'Shopping Bag : ' + currentCart._id
                        });
                    }).then(function(charge) {
                        callback(null, currentCart,charge);
                    })
                    .catch(function(err) {
                        callback(err);
                    });

            }else{
            // stripe
                stripe.customers.create({
                  source: stripeToken,
                  email: user.email,
                  description:user.displayName
                }).then(function(customer) {
                    return stripe.charges.create({
                        amount: charge, // amount in cents, again
                        currency: 'gbp',
                        customer: customer.id,
                        description:'Shopping Bag : ' + currentCart._id
                    });
                }).then(function(charge) {    

                    callback(null, currentCart,charge);
                })
                .catch(function(err) {
                    callback(err);
                });

            }
        },
        function(currentCart,charge,callback){    
           
            // create order
                order.client = {
                    name:user.displayName,
                    id:user._id,
                    ip:req.body.client_ip,
                    token:req.body.token
                };
                order.cart = currentCart._id;
                
                order.shippingAddress = user.address[0];
                
                order.status= {
                    name:'IN PROCESS',//IN PROCESS, IN DELIVERY, COMPLETE
                    description:'We have received your payment and we are preparing your order.'
                };
                order.stripe=charge;

                //save order
                order.save(function(err) {
                  if (err) {
                    callback(err);
                  } else {
                    callback(null, currentCart,order);
                  }
                });
            
            
        },
        function(currentCart_,_order,callback){
            //create new cart
            shoppingCart.save(function(err) {
              if (err) {
                callback(err);
              } else {
                callback(null, _order, shoppingCart,currentCart_);
              }
            });
        },
        function(order_, _shoppingCart, _orderCart, callback){
            
            User.findById(user._id).exec(function (err, user) {
                if (err) {
                    callback(err);
                } else if (!user) {
                    callback(err);
                }
                // update user 
                user.orders.push(order_);
                user.cart = _shoppingCart._id;
                user.stripeCustomerId = order_.stripe.customer;
                // save the user
                user.save(function (err) {
                  
                    if (err) {
                        callback(err);
                    }else{
                        // Remove sensitive data
                        user.password = undefined;
                        user.salt = undefined;
                        req.user=user;

                        var result={
                            user:user,
                            order:order_,
                            orderCart:_orderCart
                        };

                        callback(null, result);
                    }
                });
            });
        },
    ],
    // optional callback
    function(err, result){
        console.log(err);
       if (!err) {
          res.json(result);
       }else{
          res.status(400).send(err);
       }
       
    });

  // 0. stripe,
  // 1. get current user cart and mark it as inactive cart.status='not-active',save current cart
  // 2. create and save new cart
  // 3. populate order and save order
  // 4. update user and save user






  // order.save(function(err) {
  //   if (err) {
  //     return res.status(400).send({
  //       message: errorHandler.getErrorMessage(err)
  //     });
  //   } else {
  //     res.jsonp(order);
  //   }
  // });
};

/**
 * Show the current Order
 */
exports.read = function(req, res) {

    Order.findById(req.params.orderId).populate('cart.products.product').exec(function (err, order) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else if (!order) {
            return res.status(404).send({
                message: 'No order with that identifier has been found'
            });
        }
        res.jsonp(order);
    });


  // convert mongoose document to JSON
  // var order = req.order ? req.order.toJSON() : {};

  // // Add a custom field to the Article, for determining if the current User is the "owner".
  // // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  // order.isCurrentUserOwner = req.user && order.user && order.user._id.toString() === req.user._id.toString() ? true : false;

  // res.jsonp(order);
};

/**
 * Update a Order
 */
exports.update = function(req, res) {
  var order = req.order ;

  order = _.extend(order , req.body);

  order.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(order);
    }
  });
};

/**
 * Delete an Order
 */
exports.delete = function(req, res) {
  var order = req.order ;

  order.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(order);
    }
  });
};

/**
 * List of Orders
 */
exports.list = function(req, res) { 
  Order.find().sort('-created').populate('user', 'displayName').exec(function(err, orders) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(orders);
    }
  });
};

/**
 * Order middleware
 */
exports.orderByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Order is invalid'
    });
  }

  Order.findById(id).populate('user', 'displayName').exec(function (err, order) {
    if (err) {
      return next(err);
    } else if (!order) {
      return res.status(404).send({
        message: 'No Order with that identifier has been found'
      });
    }
    req.order = order;
    next();
  });
};
