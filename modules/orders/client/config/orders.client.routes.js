(function () {
  'use strict';

  angular
    .module('orders')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {


   // $stateProvider
      
      
   //    .state('settings.viewOrder', {
   //      url: '/orders/:orderId',
   //      templateUrl: 'modules/orders/client/views/view-order.client.view.html',
   //      controller: 'OrdersUserController',
   //      controllerAs: 'vm',
   //      resolve: {
   //        orderResolve: getOrder
   //      },
   //      title:'Order {{ orderResolve._id }}'
        
   //    });
  }

  getOrder.$inject = ['$stateParams', 'OrdersService'];

  function getOrder($stateParams, OrdersService) {
    return OrdersService.get({
      orderId: $stateParams.orderId
    }).$promise;
  }

  newOrder.$inject = ['OrdersService'];

  function newOrder(OrdersService) {
    return new OrdersService();
  }
})();
