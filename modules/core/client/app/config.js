'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function () {
  // Init module configuration options
  var applicationModuleName = 'mean';
  var applicationModuleVendorDependencies = ['ngResource',
   'ngAnimate', 
   'ngMessages', 
   'ngCookies', 
   'ui.router', 
   'toastr',
   'credit-cards',
   'angularPayments',
   'angularSpinner',
   'ui.bootstrap', 
   'ui.utils', 
   'angularFileUpload'];

  // Add a new vertical module
  var registerModule = function (moduleName, dependencies) {
    // Create angular module
    angular.module(moduleName, dependencies || [])
       .run(function($rootScope) {
        var $=window.$;
          $rootScope.$on('$viewContentLoaded',function(){
            $('html, body').animate({
              scrollTop: 0
            }, 0);
          });
        }); 
    // Add the module to the AngularJS configuration file
    angular.module(applicationModuleName).requires.push(moduleName);
  };

  return {
    applicationModuleName: applicationModuleName,
    applicationModuleVendorDependencies: applicationModuleVendorDependencies,
    registerModule: registerModule
  };
})();
