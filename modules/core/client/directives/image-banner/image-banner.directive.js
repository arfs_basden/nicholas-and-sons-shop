'use strict';

angular.module('core')
  .controller('ImageBannerCtrl', function ($scope,$timeout) {
    	$scope.animate = false;
    	$scope.play = function() {
      		$scope.animate = !$scope.animate;
    	};
    	$timeout($scope.play, 6000);
  })
  .directive('imageBanner', function () {
    return {
      templateUrl: 'modules/core/client/directives/image-banner/image-banner.html',
      restrict: 'E',
      controller: 'ImageBannerCtrl'
    };
  });