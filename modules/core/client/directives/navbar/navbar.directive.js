'use strict';

angular.module('core')
  .directive('navbar',function($state){
    return {
    	replace:true,
      templateUrl: 'modules/core/client/directives/navbar/navbar.html',
      restrict: 'E',
      controller: 'NavbarController',
      //controllerAs: 'nav'
    };
  });
