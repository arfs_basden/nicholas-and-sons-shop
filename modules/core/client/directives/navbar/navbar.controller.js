'use strict';

angular.module('core')
.controller('NavbarController', ['$scope', '$state', 'Authentication', 'Menus',
  function ($scope, $state, Authentication, Menus) {
    // Expose view variables
    $scope.$state = $state;
    $scope.authentication = Authentication;

    if ($scope.authentication.user !== undefined && typeof $scope.authentication.user === 'object') {
        $scope.hasRole = function (role) {
            return $scope.authentication.user.roles.indexOf(role) !== -1;
        };
    }

    

    // Get the topbar menu
    $scope.menu = Menus.getMenu('topbar');

    // Toggle the menu items
    $scope.isCollapsed2 = true;
    $scope.isCollapsed = true;
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };

    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = true;
    });
  }
]);