'use strict';

angular.module('core')
  .directive('footer', function() {
    return {
    	replace:true,
      templateUrl: 'modules/core/client/directives/footer/footer.html',
      restrict: 'E',
      link: function(scope, element) {
        element.addClass('footer');
      }
    };
  });
