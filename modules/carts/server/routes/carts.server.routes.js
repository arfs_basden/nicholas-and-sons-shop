'use strict';

/**
 * Module dependencies
 */
var cartsPolicy = require('../policies/carts.server.policy'),
  cart = require('../controllers/carts.server.controller');

module.exports = function(app) {
  // Carts Routes
  app.route('/api/cart').all(cartsPolicy.isAllowed)
    .get(cart.list)
    .post(cart.create);

  app.route('/api/cart/:cartId/remove').all(cartsPolicy.isAllowed)
    .put(cart.removeProducts);

  app.route('/api/cart/:cartId').all(cartsPolicy.isAllowed)
    .get(cart.read)
    .put(cart.addProducts);
    //.delete(carts.delete);

  // Finish by binding the Cart middleware
  app.param('cartId', cart.cartByID);
};
