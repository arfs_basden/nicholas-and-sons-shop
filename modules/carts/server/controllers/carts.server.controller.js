'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
    mongoose = require('mongoose'),
    Cart = mongoose.model('Cart'),
    async = require('async'),
    Schema = mongoose.Schema,
    errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
    _ = require('lodash');

/**
 * Create a Cart
 */
exports.create = function(req, res) {
    var cart = new Cart(req.body);
    cart.user = req.user;

    cart.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(cart);
        }
    });
};

/**
 * Show the current Cart
 * Cart with products populated
 */
exports.read = function(req, res) {

    Cart.findById(req.params.cartId).populate('products.product').exec(function (err, cart) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else if (!cart) {
            return res.status(404).send({
                message: 'No Cart with that identifier has been found'
            });
        }
        res.jsonp(cart);
    });
};


/**
 * Update Cart 
 * Remove Item(s) from a Cart
 */
exports.removeProducts = function(req, res) {
    
    var cart = req.cart ;
    var remove_products = req.body.products;

    async.series([
        function(callback){
            //update cart items
            for(var l=0; l < remove_products.length;l++){
                    
                // var cart_item = {
                var removeItemProductId = remove_products[l].product._id;
                var removeItemProductQty = remove_products[l].qty;
                // };
                console.log(removeItemProductId);
                console.log(removeItemProductQty);
                // for (var i = cart.products.length - 1; i >= 0; i--) {
                //     if(remove_products[l].product._id===cart.products[i].product.toString()){
                //          cart.products.splice(i, 1);
                //          break;
                //     }
                // }

                var _prod =_.find(cart.products, function(o,index) {
                    if(o.product.toString() === removeItemProductId){
                        o.qty=o.qty-removeItemProductQty;
                        if(o.qty===0){
                            cart.products.splice(index, 1);
                        }
                        return o;
                    }
                });
                    
                console.log(cart.products);

                //   if(o.product.toString() === remove_products[l].product._id){
                        
                //     //update qty
                //     o.qty=o.qty-remove_products[l].qty;
                        
                //     //remove item if qty = 0
                //     if(o.qty===0){
                //       cart.products.splice(index, 1);
                //     }
                        
                //   }
                // });
                
            }
            //total items
            var cart_total_items=0;
            for(var k=0; k < cart.products.length;k++){
                 cart_total_items = cart_total_items+cart.products[k].qty;
            }
            cart.totalProducts=cart_total_items;
            callback(null, { 'products-in-cart-updated':true });
        },
        function(callback){
            // save updated cart...
            cart.save(function(err) {
                if (err) {
                     callback(err);
                } else {
                    callback(null, { 'user-cart-saved':true });
                }
            });
        },    
        function(callback){
            // get cart with products populated...
            Cart.findById(cart._id).populate('products.product')
                .exec(function (err, cartPopulated) {
                    if (err) {
                        callback(err);
                    } 
                    // update cart total price
                    var cart_total_price=0;
                    for (var i = cartPopulated.products.length - 1; i >= 0; i--) {
                        var pricecount = Number(cartPopulated.products[i].qty) * Number(cartPopulated.products[i].product.price.value);
                        cart_total_price = cart_total_price + pricecount;
                    }
                    cart.totalPrice=cart_total_price;

                    callback(null, { 'total-price-in-cart-updated':true });
            });
        },
        function(callback){
            // save cart fully updated...
            cart.save(function(err) {
                if (err) {
                     callback(err);
                } else {
                    callback(null, { 'user-cart-saved':true });
                }
            });
        },
        function(callback){
                // do some more stuff ...
                Cart.findById(cart._id).populate('products.product')
                    .exec(function (err, cartFinal) {
                        if (err) {
                            callback(err);
                        } 
                        callback(null, cartFinal);
                });
            },       
    ],
    // end- 
    function(err, results){
        if (err){
            return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
            });
        }else{
            req.cart = cart;
            res.jsonp(results[4]);
        }
    });
};


/**
 * Update Cart
 * Sync cart from cookie after Auth 
 * Add Item(s) to a Cart 
 */
exports.addProducts = function(req, res) {
    
    var cart = req.cart ;
    var cookie_products = req.body.products;
    console.log(cookie_products);
    async.series([
            function(callback){
                //update cart items
                for(var l=0; l < cookie_products.length;l++){
                        
                    var cart_item = {
                        product:cookie_products[l].product._id,
                        qty:cookie_products[l].qty
                    };
                    
                    var _prod =_.find(cart.products, function(o) {
                        if(o.product.toString() === cookie_products[l].product._id){
                            o.qty=o.qty+Number(cookie_products[l].qty);
                            return o;
                        }
                    });

                    if(_prod===undefined){
                        cart.products.push(cart_item);
                    }
                }
                //total items
                var cart_total_items=0;
                for(var k=0; k < cart.products.length;k++){
                     cart_total_items = cart_total_items+cart.products[k].qty;
                }
                cart.totalProducts=cart_total_items;
                callback(null, { 'products-in-cart-updated':true });
            },
            function(callback){
                    // do some more stuff ...
                    cart.save(function(err) {
                        if (err) {
                             callback(err);
                        } else {
                            callback(null, { 'user-cart-saved':true });
                        }
                    });
            },    
            function(callback){
                    // do some more stuff ...
                    Cart.findById(cart._id).populate('products.product')
                        .exec(function (err, cartPopulated) {
                            if (err) {
                                callback(err);
                            } 
                                
                            var cart_total_price=0;
                            for (var i = cartPopulated.products.length - 1; i >= 0; i--) {
                                var pricecount = Number(cartPopulated.products[i].qty) * Number(cartPopulated.products[i].product.price.value);
                                cart_total_price = cart_total_price + pricecount;
                            }
                            cart.totalPrice=cart_total_price;

                            callback(null, { 'total-price-in-cart-updated':true });
                    });
            },
            function(callback){
                    // do some more stuff ...
                    cart.save(function(err) {
                        if (err) {
                             callback(err);
                        } else {
                            callback(null, { 'user-cart-saved':true });
                        }
                    });
            },
            function(callback){
                // do some more stuff ...
                Cart.findById(cart._id).populate('products.product')
                    .exec(function (err, cartFinal) {
                        if (err) {
                            callback(err);
                        } 
                        callback(null, cartFinal);
                });
            },    
    ],
    // optional callback
    function(err, results){
            if (err) {
                return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                });
            } else {
                req.cart = cart;
                res.jsonp(results[4]);
            }
    });
};


/**
 * List of Carts
 */
exports.list = function(req, res) { 
    Cart.find().sort('-created').populate('product.products')
        .exec(function(err, carts) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(carts);
            }
    });
};

/**
 * Cart middleware
 */
exports.cartByID = function(req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).send({
            message: 'Cart is invalid'
        });
    }

    Cart.findById(id).exec(function (err, cart) {
        if (err) {
            return next(err);
        } else if (!cart) {
            return res.status(404).send({
                message: 'No Cart with that identifier has been found'
            });
        }
        req.cart = cart;
        next();
    });
};
