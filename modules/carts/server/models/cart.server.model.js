'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Cart Schema
 */
var CartSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  status: {
    type: String,
    default: 'active',
  },
  totalProducts: {
    type: Number,
    default: 0,
  },
  totalPrice: {
    type: Number,
    default: 0,
  },
  products:[{
    qty:Number,
    product:{
      type: Schema.ObjectId,
      ref: 'Product'
    }
  }]
});

mongoose.model('Cart', CartSchema);
