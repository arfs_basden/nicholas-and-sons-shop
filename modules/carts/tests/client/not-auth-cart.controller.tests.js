(function () {
    'use strict';

    describe('Cart Guest Controller Tests', function () {
         // Initialize global variables
        var CartController,
            $scope,
            Authentication,
            $state,
            $stateParams,
            $rootScope,
            CartsService,
            ShoppingCartService,
            $uibModal,
            $cookies,
            vm,
            fakeModal,
            eventEmitted,
            mockCart;
         
            beforeEach(function() {
                    module(ApplicationConfiguration.applicationModuleName);
            });


            beforeEach(function () {
                var mockShoppingCartService = {};


                module(ApplicationConfiguration.applicationModuleName, function ($provide) {
                    $provide.value('ShoppingCartService', mockShoppingCartService);

                });

                inject(function ($q) {
                    //
                    mockShoppingCartService.data = [
                        {
                            '_id':'5781a3c914590fab2d2bf45c','__v':8,
                            'products':[
                                {
                                    'product':
                                    {
                                        '_id':'57521d35fbdb7218306cc644',
                                        'active':true,
                                        'subcategory':'rorscharch',
                                        'price':{'value':'190','currency':'£'},
                                        'stock':10,'__v':0,
                                        'reviews':[],
                                        'created':'2016-06-04T00:13:41.308Z',
                                        'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                                        'category':'women',
                                        'info':'Some info here',
                                        'description':'Some t is a long racted by the',
                                        'name':'Rorscharch Red'
                                    },
                                    'qty':1,
                                    '_id':'57854aa2da56fe5d02848188'
                                }
                            ],
                            'totalPrice':150,
                            'totalProducts':1,
                            'status':'active',
                            'created':'2016-07-10T01:24:25.122Z'
                        }
                    ];

                    mockShoppingCartService.removeItem = function(item,qty) {
                        var defer = $q.defer();
                        defer.resolve(mockShoppingCartService);
                        return defer.promise;
                    };

                    mockShoppingCartService.addItem= function (item,qty){
                        var defer = $q.defer();
                        defer.resolve(mockShoppingCartService);
                        return defer.promise;
                    };
                });
            });

   
            beforeEach(inject(function ($controller,_$cookies_,$templateCache,_$state_,_$stateParams_,_$rootScope_, _Authentication_,_CartsService_, _ShoppingCartService_,_$uibModal_) {
                $rootScope = _$rootScope_;
                $scope = $rootScope.$new();
                Authentication = _Authentication_;
                ShoppingCartService = _ShoppingCartService_;
                $state = _$state_;
                $uibModal = _$uibModal_;
                $cookies=_$cookies_;
                CartsService=_CartsService_;
                $stateParams = _$stateParams_;
                $templateCache.put('/modules/carts/client/views/cart-modal-remove.client.view.html', '');
                mockCart = new CartsService({
                    _id:1
                });

                Authentication.user = undefined;//{
                //     name: 'user',
                //     roles: ['user']
                // };

              	$cookies.putObject('ns-cart',{
              		products:[{_id:1},{_id:2},{_id:3}],
              		total_cart:'150',
              		server_products:[{_id:1},{_id:2}]

              	});

                vm = $controller('NotAuthCartController as vm',{
                    $scope: $scope,
                    $rootScope: $rootScope,
                    $state: $state,
                    $cookies:$cookies,
                    Authentication: Authentication,
                    cartResolve: mockCart,
                    $uibModal:$uibModal
                });
                $scope.$digest();
            }));

        describe('should have defined', function () {
            it('Authentication service', function () {
                expect(vm.authentication).toBeDefined();
            });
            
            it('remove function', function () {
                expect(vm.remove).toBeDefined();
            });
            it('addToCart function', function () {
                expect(vm.addToCart).toBeDefined();
            });
            it('removeFromCart function', function () {
                expect(vm.removeFromCart).toBeDefined();
            });
           
        });

        describe('when user add item to cart using spinner', function () {

            it('it should add item to the cart and emit event to sync navbar', function () {
                var item = {
                    '_id':'57521d35fbdb7218306cc644',
                    'active':true,
                    'subcategory':'rorscharch',
                    'price':{'value':'190','currency':'£'},
                    'stock':10,'__v':0,
                    'reviews':[],
                    'created':'2016-06-04T00:13:41.308Z',
                    'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                    'category':'women',
                    'info':'Some info here',
                    'description':'Some t is a long racted by the',
                    'name':'Rorscharch Red'
                };

                vm.addToCart(item,1);
                $rootScope.$apply();
                expect(vm.cart).toBeDefined();
            });

            it('it should handle error when add item to the cart', inject(function (ShoppingCartService,$q) {

                ShoppingCartService.addItem= function (item,qty){
                    var defer = $q.defer();
                    defer.reject({});
                    return defer.promise;
                };

                var item = {
                    '_id':'57521d35fbdb7218306cc644',
                    'active':true,
                    'subcategory':'rorscharch',
                    'price':{'value':'190','currency':'£'},
                    'stock':10,'__v':0,
                    'reviews':[],
                    'created':'2016-06-04T00:13:41.308Z',
                    'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                    'category':'women',
                    'info':'Some info here',
                    'description':'Some t is a long racted by the',
                    'name':'Rorscharch Red'
                };

                vm.addToCart(item,1);
                $rootScope.$apply();
            }));
        });


        describe('when user remove item from the cart using spinner', function () {
            it('it should show pop up before remove item from the cart', function () {
                var item = {
                    '_id':'57521d35fbdb7218306cc644',
                    'active':true,
                    'subcategory':'rorscharch',
                    'price':{'value':'190','currency':'£'},
                    'stock':10,'__v':0,
                    'reviews':[],
                    'created':'2016-06-04T00:13:41.308Z',
                    'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                    'category':'women',
                    'info':'Some info here',
                    'description':'Some t is a long racted by the',
                    'name':'Rorscharch Red'
                };

                vm.remove(item,1);
                $rootScope.$apply();
                expect(vm.selected).toBeDefined();
            });

            it('removeFromCart should remove item from the cart and emit event to sync navbar', function () {
                var item = {
                    '_id':'57521d35fbdb7218306cc644',
                    'active':true,
                    'subcategory':'rorscharch',
                    'price':{'value':'190','currency':'£'},
                    'stock':10,'__v':0,
                    'reviews':[],
                    'created':'2016-06-04T00:13:41.308Z',
                    'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                    'category':'women',
                    'info':'Some info here',
                    'description':'Some t is a long racted by the',
                    'name':'Rorscharch Red'
                };

                vm.removeFromCart(item,1);
                $rootScope.$apply();
            });

            it('removeFromCart should handle error when remove item from the cart', inject(function (ShoppingCartService,$q) {

                ShoppingCartService.removeItem= function (item,qty){
                    var defer = $q.defer();
                    defer.reject({});
                    return defer.promise;
                };

                var item = {
                    '_id':'57521d35fbdb7218306cc644',
                    'active':true,
                    'subcategory':'rorscharch',
                    'price':{'value':'190','currency':'£'},
                    'stock':10,'__v':0,
                    'reviews':[],
                    'created':'2016-06-04T00:13:41.308Z',
                    'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                    'category':'women',
                    'info':'Some info here',
                    'description':'Some t is a long racted by the',
                    'name':'Rorscharch Red'
                };

                vm.removeFromCart(item,1);
                $rootScope.$apply();
            }));

        });

    });
	// afterEach(inject(function(Authentication){
	// 	Authentication.user = {
 //            name: 'user',
 //            roles: ['user']
 //        };

	// }));

})();
