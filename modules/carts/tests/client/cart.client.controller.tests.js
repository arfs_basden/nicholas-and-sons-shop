(function () {
    'use strict';

    describe('Cart Controller Tests', function () {
         // Initialize global variables
        var CartController,
            $scope,
            Authentication,
            $state,
            $stateParams,
            $rootScope,
            CartsService,
            ShoppingCartService,
            $uibModal,
            vm,
            fakeModal,
            eventEmitted,
            mockCart;
         
            beforeEach(function() {
                    module(ApplicationConfiguration.applicationModuleName);
            });


            beforeEach(function () {
                var mockShoppingCartService = {};


                module(ApplicationConfiguration.applicationModuleName, function ($provide) {
                    $provide.value('ShoppingCartService', mockShoppingCartService);

                });

                inject(function ($q) {
                    //
                    mockShoppingCartService.data = [
                        {
                            '_id':'5781a3c914590fab2d2bf45c','__v':8,
                            'products':[
                                {
                                    'product':
                                    {
                                        '_id':'57521d35fbdb7218306cc644',
                                        'active':true,
                                        'subcategory':'rorscharch',
                                        'price':{'value':'190','currency':'£'},
                                        'stock':10,'__v':0,
                                        'reviews':[],
                                        'created':'2016-06-04T00:13:41.308Z',
                                        'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                                        'category':'women',
                                        'info':'Some info here',
                                        'description':'Some t is a long racted by the',
                                        'name':'Rorscharch Red'
                                    },
                                    'qty':1,
                                    '_id':'57854aa2da56fe5d02848188'
                                }
                            ],
                            'totalPrice':150,
                            'totalProducts':1,
                            'status':'active',
                            'created':'2016-07-10T01:24:25.122Z'
                        }
                    ];

                    mockShoppingCartService.removeItem = function(item,qty) {
                        var defer = $q.defer();
                        defer.resolve(mockShoppingCartService);
                        return defer.promise;
                    };

                    mockShoppingCartService.addItem= function (item,qty){
                        var defer = $q.defer();
                        defer.resolve(mockShoppingCartService);
                        return defer.promise;
                    };
                });
            });

   
            beforeEach(inject(function ($controller,$templateCache,_$state_,_$stateParams_,_$rootScope_, _Authentication_,_CartsService_, _ShoppingCartService_,_$uibModal_) {
                $rootScope = _$rootScope_;
                $scope = $rootScope.$new();
                Authentication = _Authentication_;
                ShoppingCartService = _ShoppingCartService_;
                $state = _$state_;
                $uibModal = _$uibModal_;
                CartsService=_CartsService_;
                $stateParams = _$stateParams_;
                $stateParams.cartId = 1;

                $templateCache.put('/modules/carts/client/views/cart-modal-remove.client.view.html', '');
                $rootScope.$on('$sync-cart-user', function() {
                   eventEmitted = true;
                });


                eventEmitted = false;


                mockCart = new CartsService({
                    _id:1
                });

                Authentication.user = {
                    name: 'user',
                    roles: ['user']
                };

                $stateParams.strategyId = undefined;

            //CartController.$inject = ['$scope','$rootScope', '$state', 'Authentication', 'cartResolve','ShoppingCartService','$uibModal'];
              
                vm = $controller('CartController as vm',{
                    $scope: $scope,
                    $rootScope: $rootScope,
                    $state: $state,
                    Authentication: Authentication,
                    cartResolve: mockCart,
                    ShoppingCartService: ShoppingCartService,
                    $uibModal:$uibModal
                });
                $scope.$digest();
            }));

        describe('should have defined', function () {
            it('Authentication service', function () {
                expect(vm.authentication).toBeDefined();
            });
            it('ShoppingCartService service', function () {
                expect(ShoppingCartService).toBeDefined();
            });
            
            it('remove function', function () {
                expect(vm.remove).toBeDefined();
            });
            it('addToCart function', function () {
                expect(vm.addToCart).toBeDefined();
            });
            it('removeFromCart function', function () {
                expect(vm.removeFromCart).toBeDefined();
            });
           
            it('refreshCart function', function () {
                expect(vm.refreshCart).toBeDefined();
            });

            it('cart object', function () {
                expect(vm.cart).toBeDefined();
            });

        });

        describe('when user add item to cart using spinner', function () {
            it('it should add item to the cart and emit event to sync navbar', function () {
                var item = {
                    '_id':'57521d35fbdb7218306cc644',
                    'active':true,
                    'subcategory':'rorscharch',
                    'price':{'value':'190','currency':'£'},
                    'stock':10,'__v':0,
                    'reviews':[],
                    'created':'2016-06-04T00:13:41.308Z',
                    'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                    'category':'women',
                    'info':'Some info here',
                    'description':'Some t is a long racted by the',
                    'name':'Rorscharch Red'
                };

                vm.addToCart(item,1);
                $rootScope.$apply();
                expect(eventEmitted).toBe(true);
            });

            it('it should handle error when add item to the cart', inject(function (ShoppingCartService,$q) {

                ShoppingCartService.addItem= function (item,qty){
                    var defer = $q.defer();
                    defer.reject({});
                    return defer.promise;
                };

                var item = {
                    '_id':'57521d35fbdb7218306cc644',
                    'active':true,
                    'subcategory':'rorscharch',
                    'price':{'value':'190','currency':'£'},
                    'stock':10,'__v':0,
                    'reviews':[],
                    'created':'2016-06-04T00:13:41.308Z',
                    'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                    'category':'women',
                    'info':'Some info here',
                    'description':'Some t is a long racted by the',
                    'name':'Rorscharch Red'
                };

                vm.addToCart(item,1);
                $rootScope.$apply();
                expect(eventEmitted).toBe(false);
            }));
        });


        describe('when user remove item from the cart using spinner', function () {
            it('it should show pop up before remove item from the cart', function () {
                var item = {
                    '_id':'57521d35fbdb7218306cc644',
                    'active':true,
                    'subcategory':'rorscharch',
                    'price':{'value':'190','currency':'£'},
                    'stock':10,'__v':0,
                    'reviews':[],
                    'created':'2016-06-04T00:13:41.308Z',
                    'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                    'category':'women',
                    'info':'Some info here',
                    'description':'Some t is a long racted by the',
                    'name':'Rorscharch Red'
                };

                vm.remove(item,1);
                $rootScope.$apply();
                expect(vm.selected).toBeDefined();
                expect(eventEmitted).toBe(false);
            });

            it('removeFromCart should remove item from the cart and emit event to sync navbar', function () {
                var item = {
                    '_id':'57521d35fbdb7218306cc644',
                    'active':true,
                    'subcategory':'rorscharch',
                    'price':{'value':'190','currency':'£'},
                    'stock':10,'__v':0,
                    'reviews':[],
                    'created':'2016-06-04T00:13:41.308Z',
                    'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                    'category':'women',
                    'info':'Some info here',
                    'description':'Some t is a long racted by the',
                    'name':'Rorscharch Red'
                };

                vm.removeFromCart(item,1);
                $rootScope.$apply();
                expect(vm.selected).toBeDefined();
                expect(eventEmitted).toBe(true);
            });

            it('removeFromCart should handle error when remove item from the cart', inject(function (ShoppingCartService,$q) {

                ShoppingCartService.removeItem= function (item,qty){
                    var defer = $q.defer();
                    defer.reject({});
                    return defer.promise;
                };

                var item = {
                    '_id':'57521d35fbdb7218306cc644',
                    'active':true,
                    'subcategory':'rorscharch',
                    'price':{'value':'190','currency':'£'},
                    'stock':10,'__v':0,
                    'reviews':[],
                    'created':'2016-06-04T00:13:41.308Z',
                    'image':'/modules/core/client/img/assets/designs/rorscharch/red.jpg',
                    'category':'women',
                    'info':'Some info here',
                    'description':'Some t is a long racted by the',
                    'name':'Rorscharch Red'
                };

                vm.removeFromCart(item,1);
                $rootScope.$apply();
                expect(eventEmitted).toBe(false);
            }));

        });



    });

//     // Then we can start by loading the main application module
//     beforeEach(module(ApplicationConfiguration.applicationModuleName));

//     // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
//     // This allows us to inject a service but then attach it to a variable
//     // with the same name as the service.
//     beforeEach(inject(function ($controller, $rootScope, _$state_, _$httpBackend_, _Authentication_, _CartsService_) {
//       // Set a new global scope
//       $scope = $rootScope.$new();

//       // Point global variables to injected services
//       $httpBackend = _$httpBackend_;
//       $state = _$state_;
//       Authentication = _Authentication_;
//       CartsService = _CartsService_;

//       // create mock Cart
//       mockCart = new CartsService({
//         _id: '525a8422f6d0f87f0e407a33',
//         name: 'Cart Name'
//       });

//       // Mock logged in user
//       Authentication.user = {
//         roles: ['user']
//       };

//       // Initialize the Carts controller.
//       CartsController = $controller('CartsController as vm', {
//         $scope: $scope,
//         cartResolve: {}
//       });

//       //Spy on state go
//       spyOn($state, 'go');
//     }));

//     describe('vm.save() as create', function () {
//       var sampleCartPostData;

//       beforeEach(function () {
//         // Create a sample Cart object
//         sampleCartPostData = new CartsService({
//           name: 'Cart Name'
//         });

//         $scope.vm.cart = sampleCartPostData;
//       });

//       it('should send a POST request with the form input values and then locate to new object URL', inject(function (CartsService) {
//         // Set POST response
//         $httpBackend.expectPOST('api/carts', sampleCartPostData).respond(mockCart);

//         // Run controller functionality
//         $scope.vm.save(true);
//         $httpBackend.flush();

//         // Test URL redirection after the Cart was created
//         expect($state.go).toHaveBeenCalledWith('carts.view', {
//           cartId: mockCart._id
//         });
//       }));

//       it('should set $scope.vm.error if error', function () {
//         var errorMessage = 'this is an error message';
//         $httpBackend.expectPOST('api/carts', sampleCartPostData).respond(400, {
//           message: errorMessage
//         });

//         $scope.vm.save(true);
//         $httpBackend.flush();

//         expect($scope.vm.error).toBe(errorMessage);
//       });
//     });

//     describe('vm.save() as update', function () {
//       beforeEach(function () {
//         // Mock Cart in $scope
//         $scope.vm.cart = mockCart;
//       });

//       it('should update a valid Cart', inject(function (CartsService) {
//         // Set PUT response
//         $httpBackend.expectPUT(/api\/carts\/([0-9a-fA-F]{24})$/).respond();

//         // Run controller functionality
//         $scope.vm.save(true);
//         $httpBackend.flush();

//         // Test URL location to new object
//         expect($state.go).toHaveBeenCalledWith('carts.view', {
//           cartId: mockCart._id
//         });
//       }));

//       it('should set $scope.vm.error if error', inject(function (CartsService) {
//         var errorMessage = 'error';
//         $httpBackend.expectPUT(/api\/carts\/([0-9a-fA-F]{24})$/).respond(400, {
//           message: errorMessage
//         });

//         $scope.vm.save(true);
//         $httpBackend.flush();

//         expect($scope.vm.error).toBe(errorMessage);
//       }));
//     });

//     describe('vm.remove()', function () {
//       beforeEach(function () {
//         //Setup Carts
//         $scope.vm.cart = mockCart;
//       });

//       it('should delete the Cart and redirect to Carts', function () {
//         //Return true on confirm message
//         spyOn(window, 'confirm').and.returnValue(true);

//         $httpBackend.expectDELETE(/api\/carts\/([0-9a-fA-F]{24})$/).respond(204);

//         $scope.vm.remove();
//         $httpBackend.flush();

//         expect($state.go).toHaveBeenCalledWith('carts.list');
//       });

//       it('should should not delete the Cart and not redirect', function () {
//         //Return false on confirm message
//         spyOn(window, 'confirm').and.returnValue(false);

//         $scope.vm.remove();

//         expect($state.go).not.toHaveBeenCalled();
//       });
//     });
//   });
})();
