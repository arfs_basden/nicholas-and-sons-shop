(function () {
  'use strict';

  angular
    .module('carts')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('cart', {
        abstract: true,
        url: '/cart',
        template: '<ui-view/>'
      })
      .state('cart.list', {
        url: '',
        templateUrl: 'modules/carts/client/views/list-carts.client.view.html',
        controller: 'CartsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Cart List'
        }
      })
      .state('cart.guest', {
        url: '/guest',
        templateUrl: 'modules/carts/client/views/not-auth-cart.client.view.html',
        controller: 'NotAuthCartController',
        controllerAs: 'vm',
       
      })
      .state('cart.view', {
        url: '/:cartId',
        templateUrl: 'modules/carts/client/views/form-cart.client.view.html',
        controller: 'CartController',
        controllerAs: 'vm',
        resolve: {
          cartResolve: getCart
        },  
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Cart {{ cartResolve.name }}'
        }
      })
      ;
  }

  getCart.$inject = ['$stateParams', 'CartsService'];

  function getCart($stateParams, CartsService) {
    return CartsService.get({
      cartId: $stateParams.cartId
    }).$promise;
  }

  newCart.$inject = ['CartsService'];

  function newCart(CartsService) {
    return new CartsService();
  }
})();
