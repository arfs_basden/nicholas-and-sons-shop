'use strict';

angular.module('carts')
  .directive('nsCount', function() {
    return {
        template: '<span >{{totalProducts}}</span>',
        restrict: 'E',
        scope:{
        	 totalProducts:'='
    },
    link: function(scope, element) {

      	scope.$watch('totalProducts', function(data) {
    		if(scope.totalProducts!==undefined){
    			scope.resolveClass();
    		}
    	});
      	scope.resolveClass=function(){

      		element[0].style.position='absolute';
      		element[0].style.marginTop='-17px';
      		element[0].style.color='#bbb';
      		element[0].style.fontSize='11px';
         // element[0].style.backgroundColor='black';
            element[0].style.opacity='0.6';
      		if(scope.totalProducts>9){
      			element[0].style.paddingLeft='8px';
      		}else{
      			element[0].style.paddingLeft='10px';
      		}
      	};
    }
    };
  });
