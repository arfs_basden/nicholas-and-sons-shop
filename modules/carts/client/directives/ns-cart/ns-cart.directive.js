(function () {
    'use strict';
        angular
          .module('carts')
          .directive('nsCart', nsCart);  
     /**
     * @name nsCart
     * @desc <ns-cart> Directive
     */
    function nsCart () {

        /**
        * @name nsCartController
        * @desc nsCart Controller
        * @type {Function}
        */
        nsCartController.$inject = ['$rootScope', '$cookies', 'Authentication', 'ShoppingCartService'];

        function nsCartController($rootScope, $cookies, Authentication, ShoppingCartService) {
            /*jshint validthis: true */
            var vc = this;
            vc.cookieCart=$cookies.getObject('ns-cart');
            vc.cartItems=[];
            vc.authentication=Authentication;
            vc.totalPrice=0;
            vc.totalProducts=0;

            vc.getCookieCart=getCookieCart;
            vc.getUserCart=getUserCart;


            if(Authentication.user !== undefined && typeof Authentication.user === 'object'){
                vc.getUserCart();
            }else{
                if(vc.cookieCart !== undefined){
                    vc.getCookieCart();            
                }
            }

            //sync auth
            $rootScope.$on('$sync-cart-auth',function(ev,data){
                if(Authentication.user !== undefined && typeof Authentication.user === 'object'){
                    vc.getUserCart();
                }
            }); 

            //sync cart page
            $rootScope.$on('$sync-cart-user',function(ev,data){
                vc.cartItems=data.products;
                vc.totalPrice=data.totalPrice;
                vc.totalProducts=data.totalProducts;
            }); 

            //cookie
            $rootScope.$on('$add-cart-item',function(ev,data){
                vc.cartItems=data.products;
                vc.totalPrice=data.total_cart;
                vc.totalProducts=data.server_products.length;
            });

            $rootScope.$on('$remove-cart-item',function(ev,data){
                vc.cartItems=data.products;
                vc.totalPrice=data.total_cart;
                vc.totalProducts=data.server_products.length;
            });



            function getUserCart(){
                ShoppingCartService.getShoppingCart()
                    .then(function(res){
                        //console.log(res.data);
                        vc.cartItems=res.products;
                        vc.totalPrice=res.totalPrice;
                        vc.totalProducts=res.totalProducts;
                    })
                    .catch(function(err){
                        console.log(err);
                    });
            }

            function getCookieCart(){
                vc.cartItems=$cookies.getObject('ns-cart').products;
                vc.totalPrice=$cookies.getObject('ns-cart').total_cart;
                vc.totalProducts=$cookies.getObject('ns-cart').server_products.length;
            }
        }

        /**
        * @name link
        * @desc nsCart Link
        * @type {Function}
        */
        function link() {

        }

        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'modules/carts/client/directives/ns-cart/ns-cart.html',
            controllerAs: 'vc',
            controller: nsCartController,
            bindToController: true
        };

}
 















    // angular.module('carts')
    //     .controller('nsCartController',nsCartController)
    //     .directive('nsCart', function() {
    //         return {
    //             templateUrl: 'modules/carts/client/directives/ns-cart/ns-cart.html',
    //             restrict: 'E',
    //             controller: 'nsCartController',
    //             controllerAs:'vc'
    //         };
    //     })
    //     ;



    //     nsCartController.$inject = ['$scope', '$rootScope','$cookies','Authentication', 'ShoppingCartService'];


    //     function nsCartController($scope, $rootScope,$cookies,Authentication,ShoppingCartService) {
            
    //         var vcart = this;

    //         vc.cookieCart=$cookies.getObject('ns-cart');

    //         vc.cartItems=[];
    //         vc.totalPrice=0;
    //         vc.totalProducts=0;

    //         vc.getUserCart=getUserCart;
            


    //         function getUserCart(){
    //             ShoppingCartService.getShoppingCart()
    //                 .then(function(res){
    //                     //console.log(res.data);
    //                     $scope.cartItems=res.products;
    //                     $scope.totalPrice=res.totalPrice;
    //                     $scope.totalProducts=res.totalProducts;
    //                 })
    //                 .catch(function(err){
    //                     console.log(err);
    //                 });
    //         }




    //         if(Authentication.user !== undefined && typeof Authentication.user === 'object'){
    //                 vc.getUserCart();
    //         }else{
    //             if(vc.cookieCart !== undefined){
    //                 //console.log($cookies.getObject('ns-cart'));
    //                 $scope.cartItems=$cookies.getObject('ns-cart').products;
    //                 $scope.totalPrice=$cookies.getObject('ns-cart').total_cart;
    //                 $scope.totalProducts=$cookies.getObject('ns-cart').server_products.length;
    //             }
    //         }



    //         //sync
    //         $rootScope.$on('$sync-cart-auth',function(ev,data){
    //             if(Authentication.user !== undefined && typeof Authentication.user === 'object'){
    //                 $scope.getUserCart();
    //             }
    //         }); 

    //         //cookie
    //         $rootScope.$on('$add-cart-item',function(ev,data){
    //             console.log(data);
    //             $scope.cartItems=data.products;
    //             $scope.totalPrice=data.total_cart;
    //             $scope.totalProducts=data.server_products.length;
    //         });
            
            



    //     }


})();



