//Shopping carts service used to communicate Shopping carts REST endpoints
(function () {
  'use strict';
  
   ShoppingCartService.$inject = ['$location', '$http', '$cookies','$rootScope', '$q', 'Authentication','$window'];
  
  function ShoppingCartService($location, $http, $cookies, $rootScope, $q, Authentication, $window) {  

  var shoppingCartService = {
        
        /**
        * sync cart on sign up / sign in
        */
        syncShoppingCart:function(){
            
            //get cart
            var cookie_cart = $cookies.getObject('ns-cart');
            
            // 1. no cookie exists : do nothing
            if(cookie_cart === undefined){
                $rootScope.$emit('$sync-cart-auth', true);
                return;              
            // if cookie : sync with user's cart and delete cookie
            } else {
                //payload
                var payload={
                    products:cookie_cart.products
                };
                //sync cart
                $http.put('/api/cart/'+Authentication.user.cart, payload)
                    .then(function(res2){
                        //remove cookie
                        $cookies.remove('ns-cart');
                        //update ns-cart directive
                        $rootScope.$emit('$sync-cart-auth', true);
                    })
                    .catch(function(err) {
                         console.log(err);
                    }); 
            }
        },
        
        /**
        * fetch cart
        */
        getShoppingCart:function(){
            return $http.get('/api/cart/' + Authentication.user.cart)
                .then(function(res){
                    return res.data;
                })
                .catch(function(err) {
                    return $q.reject(err.data);
                });
        },

        getShoppingCartById:function(id){
            return $http.get('/api/cart/' + id)
                .then(function(res){
                    return res.data;
                })
                .catch(function(err) {
                    return $q.reject(err.data);
                });
        },
        
        /**
        * add item to cart
        */
        addItem: function(item, qty){
            var total_cart=0;
            var cartItem={
                product:{
                    _id:item._id,
                    name:item.name,
                    price:item.price,
                    image:item.image,
                }, 
                qty:qty
            };  
            //if logged in ,item to user-cart            
            if(Authentication.user !== undefined && typeof Authentication.user === 'object'){

                var payload={
                    products:[cartItem]
                };

                return $http.put('/api/cart/' + Authentication.user.cart, payload)
                    .then(function(res2){
                        $rootScope.$emit('$sync-cart-auth', true);
                        return res2.data;
                    })
                    .catch(function(err){
                        console.log(err);
                    });
            //if not logged in ,item to cookie 
            }else{

                var server_products=[];
                for(var z=0; z < qty;z++){
                    server_products.push(cartItem.product._id);
                }

                //if no cookie
                if($cookies.getObject('ns-cart') === undefined){
                    var ns_cart = {
                        user: Authentication.user._id || 'guest',
                        products:[cartItem],
                        total_cart:cartItem.product.price.value,
                        server_products:server_products
                    };
                    //create cookie with item
                    //var qty_updated_msg1= 'Item ' + cartItem.product.name + ' added to cart';
                    //toastr.success(qty_updated_msg1, 'CART UPDATED!');
                    $rootScope.$emit('$add-cart-item', ns_cart); 
                    $cookies.putObject('ns-cart', ns_cart);
                    
                    
                //if cookie exists    
                }else{
                    
                    var ns_cook = $cookies.getObject('ns-cart');
                    var itemInCart=false;
                    //if item exists , update qty
                    for(var i=0; i < ns_cook.products.length;i++){
                        if(ns_cook.products[i].product._id===cartItem.product._id){

                            var updatedQty = Number(ns_cook.products[i].qty) + Number(cartItem.qty);
                            ns_cook.products[i].qty= updatedQty;
                            //var qty_updated_msg2= ns_cook.products[i].name + ' added to cart  quantity : ' + updatedQty;
                            //toastr.success(qty_updated_msg2, 'CART UPDATED!'); 
                            itemInCart=true;
                            break;
                        }
                    }
                    
                    // if item not exists add it 
                    if(!itemInCart){
                        ns_cook.products.push(cartItem);
                        //var qty_updated_msg3= 'Item ' + ns_cook.products[i].name + ' added to cart';
                        //toastr.success(qty_updated_msg3, 'CART UPDATED!'); 
                    }
                    
                    //add item to server_products
                    ns_cook.server_products.push(cartItem.product._id);
                    
                    //update total in cart
                    for(var k=0; k < ns_cook.products.length;k++){
                        total_cart+= Number(ns_cook.products[k].qty * ns_cook.products[k].product.price.value);
                    }
                    
                    var ns_cart1 = {
                        user: Authentication.user._id || 'guest',
                        products:ns_cook.products,
                        total_cart:total_cart,
                        server_products:ns_cook.server_products
                    };
                    
                    $rootScope.$emit('$add-cart-item', ns_cart1);
                    $cookies.putObject('ns-cart', ns_cart1);
                    
                }
                var deferred =$q.defer();
                deferred.resolve({ err:'user not auth : saved on cookie' });
                return deferred.promise;
                    
                }
        }, 
            
        /**
         * remove item form cart
        */
        removeItem: function(item,qty){
            var total_cart=0;
            var cartItem={
                product:{
                    _id:item._id,
                    name:item.name,
                    price:item.price,
                }, 
                qty:qty
            };    
            //if logged in item to user-cart            
            if(Authentication.user !== undefined && typeof Authentication.user === 'object'){

                var payload={
                    products:[cartItem]
                };

                return $http.put('/api/cart/'+Authentication.user.cart+'/remove', payload)
                    .then(function(res) {
                        return res.data;
                    })
                    .catch(function(err) {
                        return $q.reject(err.data);
                    });
            //if not logged in  remove item from cookie 
            }else{
                

                var ns_cook = $cookies.getObject('ns-cart');

                for(var i=0; i < ns_cook.products.length;i++){
                    if(ns_cook.products[i].product._id===cartItem.product._id){

                        var updatedQty = Number(ns_cook.products[i].qty) - Number(cartItem.qty);
                        ns_cook.products[i].qty= updatedQty;

                        if(updatedQty === 0){
                            ns_cook.products.splice(i,1);
                        }
                    }
                }  

                //update serverproducts cart
                var new_server_products=[];
                for(var h=0; h < ns_cook.products.length;h++){

                    for (var m = ns_cook.products[h].qty - 1; m >= 0; m--) {
                        new_server_products.push(ns_cook.products[h].product._id);
                    }
                    
                }
                //update total in cart
                for(var k=0; k < ns_cook.products.length;k++){
                    total_cart+= Number(ns_cook.products[k].qty * ns_cook.products[k].product.price.value);
                }
                
                var ns_cart2 = {
                    user: Authentication.user._id || 'guest',
                    products:ns_cook.products,
                    total_cart:total_cart,
                    server_products:new_server_products
                };
                $rootScope.$emit('$remove-cart-item', ns_cart2);
                $cookies.putObject('ns-cart', ns_cart2);
            
                var deferred =$q.defer();
                deferred.resolve({ err:'user not auth : saved on cookie' });
                return deferred.promise;
            }
        }  
    };
    
    return shoppingCartService;
} 
  
  angular.module('carts')
  .factory('ShoppingCartService', ShoppingCartService);
  
})();
