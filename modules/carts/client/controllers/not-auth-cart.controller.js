(function () {
    'use strict';

    // Carts controller
    angular.module('carts')
        .controller('NotAuthCartController', NotAuthCartController);

    NotAuthCartController.$inject = ['$scope','$rootScope', '$state','$cookies', 'Authentication','ShoppingCartService','$uibModal'];
    function NotAuthCartController ($scope, $rootScope, $state, $cookies,Authentication, ShoppingCartService, $uibModal) {
        var vm = this;

        vm.authentication = Authentication;
        //vm.cart = cart;
        vm.error = null;
        vm.remove = remove;
        vm.selected =null;
        vm.removeFromCart = removeFromCart;
        vm.addToCart = addToCart;
        vm.getCookieCart=getCookieCart;
		vm.totalPrice=null;
		vm.totalProducts=null;
      
        vm.cart =null;
  		vm.getCookieCart();
        
         if (vm.authentication.user !== undefined && typeof vm.authentication.user === 'object') {
            $state.go('cart.view',{ cartId:vm.authentication.user.cart });
         }        

        function getCookieCart(){
        	vm.cart = $cookies.getObject('ns-cart');
        	if(vm.cart!==undefined){
        		vm.cartItems=$cookies.getObject('ns-cart').products;
            	vm.totalPrice=$cookies.getObject('ns-cart').total_cart;
            	vm.totalProducts=$cookies.getObject('ns-cart').server_products.length;
        	}
        }

        // Remove existing Cart
        function remove(item,qty) {
            
            vm.selected = item;
            
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/modules/carts/client/views/cart-modal-remove.client.view.html',
                //controller: 'ModalInstanceCtrl',
                size: 'md',
                controllerAs:'vm',
                controller:'ModalRemoveController',
                resolve: {
                    selectedItem: function () {
                      return item;
                    },
                    qty: function () {
                      return qty;
                    }
                }
            });

            modalInstance.result.then(function () {
                vm.removeFromCart(item,qty);
                return;
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
        }

        function addToCart(item,qty) {
            ShoppingCartService.addItem(item,qty)
                .then(function(res){
                    vm.getCookieCart();
                })
                .catch(function(err){
                    vm.error = err.message;
                });
        }

        function removeFromCart(item,qty) {
            ShoppingCartService.removeItem(item,qty)
                .then(function(res){
                    vm.getCookieCart();
                })
                .catch(function(err){
                    vm.error = err.message;
                });
        }
    }
    
})();

