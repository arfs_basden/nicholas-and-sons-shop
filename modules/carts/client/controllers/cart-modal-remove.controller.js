(function () {
    'use strict';

    // Carts controller
    angular
        .module('carts')
        .controller('ModalRemoveController', ModalRemoveController);

	ModalRemoveController.$inject = ['$scope', '$uibModalInstance','selectedItem','qty'];

    function ModalRemoveController($scope,$uibModalInstance,selectedItem,qty){
    	var vm=this;
    	
    	vm.selectedItem=selectedItem;
    	vm.qty=qty;
    	vm.subtotal=Number(vm.qty)*Number(vm.selectedItem.price.value);
    	vm.ok = function () {
            $uibModalInstance.close();
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }
})();

