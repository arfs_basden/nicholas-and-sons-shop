(function () {
    'use strict';

    // Carts controller
    angular
        .module('carts')
        .controller('CartController', CartController);

    CartController.$inject = ['$scope','$rootScope', '$state', 'Authentication', 'cartResolve','ShoppingCartService','$uibModal'];

    function CartController ($scope, $rootScope, $state, Authentication, cart, ShoppingCartService, $uibModal) {
        var vm = this;

        vm.authentication = Authentication;
        vm.cart = cart;
        vm.error = null;
        vm.remove = remove;
        vm.selected =null;
        vm.removeFromCart = removeFromCart;
        vm.addToCart = addToCart;
        vm.refreshCart=refreshCart;


        // Remove existing Cart
        function remove(item,qty) {
            
            vm.selected = item;
            
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/modules/carts/client/views/cart-modal-remove.client.view.html',
                size: 'md',
                controllerAs:'vm',
                controller:'ModalRemoveController',
                resolve: {
                    selectedItem: function () {
                      return item;
                    },
                    qty: function () {
                      return qty;
                    }
                }
            });

            modalInstance.result.then(function () {
                vm.removeFromCart(item,qty);
                return;
            }, function () {
                // console.info('Modal dismissed at: ' + new Date());
            });
            
        }

        function addToCart(item,qty) {
            ShoppingCartService.addItem(item,qty)
                .then(function(res){
                    vm.refreshCart(res);
                })
                .catch(function(err){
                    vm.error = err.message;
                });
        }

        function removeFromCart(item,qty) {
            ShoppingCartService.removeItem(item,qty)
                .then(function(res){
                    vm.refreshCart(res);
                })
                .catch(function(err){
                    vm.error = err.message;
                });
        }

        function refreshCart(res) {
            vm.cart=res;
            $rootScope.$emit('$sync-cart-user', res);
        }
    }
})();
