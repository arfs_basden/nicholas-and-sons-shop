//wishlist service
(function () {
  'use strict';
  
   WishService.$inject = ['$http', 'Authentication','toastr'];
  
  function WishService($http, Authentication, toastr) {  

  	var wishService = {
        /**
        * add item to wishlist
        */
        getWishList: function(){
            
            return $http.get('/api/users/wish')
                .then(function(res2){
                    //console.log('get item wish');
                    //Authentication.user=res2.data;
                    //toastr.success('Item added to your wishlist', 'WISHLIST UPDATED!');
                    return res2.data;
                })
                .catch(function(err){
                    console.log(err);
                });
        },
        /**
        * add item to wishlist
        */
        addItem: function(id){
            
            return $http.get('/api/products/' + id +'/wish')
                .then(function(res2){
                    //console.log('add item wish');
                    //Authentication.user=res2.data;
                    toastr.success('Item added to your wishlist', 'WISHLIST UPDATED!');
                    return res2.data;
                })
                .catch(function(err){
                    console.log(err);
                });
        },
        /**
        * remove item to wishlist
        */
        removeItem : function(id){
            
            return $http.get('/api/products/' + id +'/wish?toggle='+1)
                .then(function(res2){
                    //console.log('add item wish');
                    //Authentication.user=res2.data;
                   	toastr.success('Item removed from your wishlist', 'WISHLIST UPDATED!');
                    return res2.data;
                })
                .catch(function(err){
                    console.log(err);
                });
        }

    };
    return wishService;
} 
  
  angular.module('products')
  .factory('WishService', WishService);
  
})();

