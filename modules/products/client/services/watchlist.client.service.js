//wishlist service
(function () {
  'use strict';
  
   WatchListService.$inject = ['$http', 'Authentication','toastr'];
  
  function WatchListService($http, Authentication, toastr) {  

  	var watchListService = {
        
        /**
        * add item to wishlist
        */
        addItem: function(id){
            
            return $http.get('/api/products/' + id +'/watch')
                .then(function(res2){
                    //console.log('add item watch');
                    Authentication.user=res2.data;
                    toastr.success('Item added to your watchlist. You will receive notifications when the item is available.', 'WATCHLIST UPDATED!');
                    return res2.data;
                })
                .catch(function(err){
                    console.log(err);
                });
        },
        

        /**
        * remove item to wishlist
        */
        removeItem : function(id){
            
            return $http.get('/api/products/' + id +'/watch?toggle='+1)
                .then(function(res2){
                    //console.log('add item watch');
                    Authentication.user=res2.data;
                   	toastr.success('Item removed from your watchlist', 'WATCHLIST UPDATED!');
                    return res2.data;
                })
                .catch(function(err){
                    console.log(err);
                });
        }

    };
    return watchListService;
} 
  
  angular.module('products')
  .factory('WatchListService', WatchListService);
  
})();

