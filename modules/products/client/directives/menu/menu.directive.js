'use strict';

angular.module('products')
 .controller('ShopMenuCtrl', function ($scope,$rootScope) {
    $scope.filterBy=function(filter){
       $scope.selectedFilter=filter; 
       $rootScope.$emit('$ns-filter-by',filter);
    };
  })
  .directive('shopMenu', function() {
    return{
      templateUrl: 'modules/products/client/directives/menu/menu.html',
      restrict: 'E',
      controller: 'ShopMenuCtrl',
    };
    
   // controllerAs: 'nav'
  });
