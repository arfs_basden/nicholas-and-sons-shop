'use strict';

angular.module('products')
 .controller('ShopHeaderCtrl', function ($scope,$state) {
    $scope.image=$scope.pic || '/modules/core/client/img/assets/home.jpg';
    //$scope.image='https://placehold.it/1200x600';
  })
  .directive('shopHeader', function() {
    return{
      templateUrl: 'modules/products/client/directives/shop-header/shop-header.html',
      restrict: 'E',
      scope:{
      	claim:'=',
      	logo:'=',
        pic:'='
      },
      controller: 'ShopHeaderCtrl',
    };
    
   // controllerAs: 'nav'
  });
