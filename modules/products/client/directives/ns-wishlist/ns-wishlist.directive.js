(function () {
	'use strict';
		angular
			.module('products')
			.directive('nsWish', nsWish);  
	 /**
	 * @name nsWish
	 * @desc <ns-cart> Directive
	 */
	function nsWish () {

		nsWishController.$inject = ['Authentication','WishService'];

		function nsWishController(Authentication,WishService){
			 /*jshint validthis: true */
			var vm=this;
			vm.authentication = Authentication;
			vm.isWishList=isWishList;
			vm.userWishlist=vm.authentication.user.wishlist; 
			
			vm.AddWishList=AddWishList;
			vm.removeWishList=removeWishList;
			//
		   

			function AddWishList(id){
			  	WishService.addItem(id)
					.then(function(res){
						//console.log(res);
						vm.authentication.user=res;
						vm.userWishlist=res.wishlist;
						//vm.toggle=vm.isWishList();
						//Authentication.user=res;
					})
					.catch(function(err){
						console.log(err);
					});
			}
			function removeWishList(id){
			  	WishService.removeItem(id)
					.then(function(res){
						//console.log(res);
						vm.authentication.user=res;
						vm.userWishlist=res.wishlist;
						
					})
					.catch(function(err){
						console.log(err);
					});
			}
			function isWishList(id){
			  	return vm.userWishlist.indexOf(id)!==-1;
			}


		}

		return {
			restrict: 'E',
			scope:{
				product:'=',
				btnSize:'='
			},
			templateUrl: 'modules/products/client/directives/ns-wishlist/ns-wishlist.html',
			controllerAs: 'vm',
			controller: nsWishController,
			bindToController: true
		};
	
}
})();