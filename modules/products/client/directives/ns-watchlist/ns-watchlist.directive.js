(function () {
    'use strict';
        angular
          .module('products')
          .directive('nsWatchlist', nsWatchlist);  
     
    function nsWatchlist () {

        nsWatchController.$inject = ['Authentication', 'WatchListService'];

        function nsWatchController(Authentication, WatchListService) {
             /*jshint validthis: true */
            var vm=this;
            vm.authentication = Authentication;
            vm.userWatchlist=vm.authentication.user.watchlist; 
            vm.AddWatchList=AddWatchList;
            vm.removeWatchList=removeWatchList;
            vm.isWatchList=isWatchList;

            function AddWatchList(id){
              WatchListService.addItem(id)
                .then(function(res){
                    //console.log(res);
                    vm.authentication.user=res;
                    vm.userWatchlist=res.watchlist;
                })
                .catch(function(err){
                    console.log(err);
                });
            }

            function removeWatchList(id){
              WatchListService.removeItem(id)
                .then(function(res){
                    //console.log(res);
                    vm.authentication.user=res;
                    vm.userWatchlist=res.watchlist;
                })
                .catch(function(err){
                    console.log(err);
                });
            }

            function isWatchList(id){
              return vm.userWatchlist.indexOf(id)!==-1;
            }
        }

        return {
            restrict: 'E',
            scope:{
                product:'=',
                btnSize:'='
            },
            templateUrl: 'modules/products/client/directives/ns-watchlist/ns-watchlist.html',
            controllerAs: 'vm',
            controller: nsWatchController,
            bindToController: true
        };
	
    }
})();