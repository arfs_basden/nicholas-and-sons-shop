(function () {
  'use strict';

  angular
    .module('products')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('products', {
        abstract: true,
        url: '/products',
        template: '<ui-view/>'
      })
      .state('products.stockist', {
        //abstract: true,
        url: '/stockist',
        template: '<stockist></stockist>'
      })
      .state('products.list', {
        url: '',
        template: '<products-list></products-list>',
        data: {
          pageTitle: 'Products List'
        }
      })
      .state('products.collections', {
        //abstract: true,
        url: '/men',
        template: '<men></men>'
      })
      .state('products.view', {
        url: '/:productId',
        templateUrl: 'modules/products/client/views/view-product.client.view.html',
        controller: 'ProductsController',
        controllerAs: 'vm',
        resolve: {
          productResolve: getProduct
        },
        data:{
          pageTitle: 'Product {{ articleResolve.name }}'
        }
      })
      .state('products.create', {
        url: '/create',
        templateUrl: 'modules/products/client/views/form-product.client.view.html',
        controller: 'ProductsController',
        controllerAs: 'vm',
        resolve: {
          productResolve: newProduct
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle : 'Products Create'
        }
      })
      .state('products.edit', {
        url: '/:productId/edit',
        templateUrl: 'modules/products/client/views/form-product.client.view.html',
        controller: 'ProductsController',
        controllerAs: 'vm',
        resolve: {
          productResolve: getProduct
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Product {{ productResolve.name }}'
        }
      })

      

      // .state('men', {
      //   url: '/men/:subcategory',
      //   template: '<men-collection></men-collection>'
      // })
      .state('products.men', {
        url: '/:category/:subcategory',
        template: '<men-collection></men-collection>',
      })
      .state('products.women', {
        url: '/:category/:subcategory',
        template: '<men-collection></men-collection>'
      })
      


      ;
  }

  getProduct.$inject = ['$stateParams', 'ProductsService'];

  function getProduct($stateParams, ProductsService) {
    return ProductsService.get({
      productId: $stateParams.productId
    }).$promise;
  }

  newProduct.$inject = ['ProductsService'];

  function newProduct(ProductsService) {
    return new ProductsService();
  }
})();
