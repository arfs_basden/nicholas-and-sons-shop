(function () {
    'use strict';

    angular.module('products')
        .component('productsList', {
            templateUrl: 'modules/products/client/views/list-products.client.view.html',
            controller: ProductsListController,
            controllerAs:'vm'
        });

    ProductsListController.$inject = ['ProductsService','$rootScope','ShoppingCartService','Authentication','WishService','WatchListService'];

    function ProductsListController(ProductsService,$rootScope, ShoppingCartService,Authentication,WishService,WatchListService) {
        var vm = this;
        vm.authentication = Authentication;
        vm.products = ProductsService.query();
        vm.filterBy={ subcategory:'' };
        vm.pics=vm.products;
        $rootScope.$on('$ns-filter-by',function(e,data){
             vm.filterBy.subcategory= data;
        });

        vm.designs=[
            {
                _id:1,
                name:'Fingerprint Stone',
                subcategory:'fingerprint',
                image:'/modules/core/client/img/assets/designs/fingerprint/fingerprintStoneDemo.jpg'
            },
             {
                _id:2,
                name:'Fingerprint Maroon',
                subcategory:'fingerprint',
                image:'/modules/core/client/img/assets/designs/fingerprint/fingerprintMaroonDemo.jpg'
            },
             {
                _id:3,
                name:'Fingerprint Orange',
                subcategory:'fingerprint',
                image:'/modules/core/client/img/assets/designs/fingerprint/fingerprintOrangeDemo.jpg'
            },
             {
                _id:4,
                name:'Fingerprint Grey',
                subcategory:'fingerprint',
                image:'/modules/core/client/img/assets/designs/fingerprint/fingerprintGreyDemo.jpg'
            },
             {
                _id:5,
                name:'Fingerprint White',
                subcategory:'fingerprint',
                image:'/modules/core/client/img/assets/designs/fingerprint/fingerprintWhiteDemo.jpg'
            },
            
            {
                _id:6,
                name:'Alligator Black',
                subcategory:'alligator',
                image:'/modules/core/client/img/assets/designs/alligator/alligatorBlackDemo.jpg'
            },
             {
                _id:7,
                name:'Alligator Brown',
                subcategory:'alligator',
                image:'/modules/core/client/img/assets/designs/alligator/alligatorBrownDemo.jpg'
            },
             {
                _id:8,
                name:'Alligator Olive',
                subcategory:'alligator',
                image:'/modules/core/client/img/assets/designs/alligator/alligatorOliveDemo.jpg'
            },
             {
                _id:9,
                name:'Alligator Grey',
                subcategory:'alligator',
                image:'/modules/core/client/img/assets/designs/alligator/alligatorGreyDemo.jpg'
            },
             {
                _id:10,
                name:'Alligator White',
                subcategory:'alligator',
                image:'/modules/core/client/img/assets/designs/alligator/alligatorWhiteDemo.jpg'
            },
             {
                _id:11,
                name:'Yatching Color',
                subcategory:'yatching',
                image:'/modules/core/client/img/assets/designs/yatching/yatch.jpg'
            },
            {
                _id:12,
                name:'Yatching B&W',
                subcategory:'yatching',
                image:'/modules/core/client/img/assets/designs/yatching/yatchbw.jpg'
            },
            {
                _id:13,
                name:'Rorscharch Red',
                subcategory:'rorscharch',
                image:'/modules/core/client/img/assets/designs/rorscharch/red.jpg'
            },
            {
                _id:14,
                name:'Rorscharch Blue',
                subcategory:'rorscharch',
                image:'/modules/core/client/img/assets/designs/rorscharch/blue.jpg'
            },
        ];
        vm.addToCart=function(item,qty) {
            ShoppingCartService.addItem(item,qty)
                .then(function(res){
                        //console.log(res);
                })
                .catch(function(err){
                        this.errors.other = err.message;
                });
        };
    }
})();
