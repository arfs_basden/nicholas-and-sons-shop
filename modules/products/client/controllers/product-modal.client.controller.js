(function () {
    'use strict';

    // Carts controller
    angular
        .module('products')
        .controller('ProductModalController', ProductModalController);

	ProductModalController.$inject = ['$scope', '$uibModalInstance','selectedItem'];

    function ProductModalController($scope,$uibModalInstance,selectedItem){
    	var vm=this;
    	vm.selectedItem=selectedItem;
    	
    	vm.ok = function () {
            $uibModalInstance.close();
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }
})();


