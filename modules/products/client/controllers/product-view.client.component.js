(function () {
  'use strict';

  // Products controller
  angular
    .module('products')
    .controller('ProductsController', ProductsController);

  ProductsController.$inject = ['$scope','$state', '$uibModal','productResolve','ShoppingCartService','ProductsService'];

  function ProductsController ($scope, $state, $uibModal,product, ShoppingCartService,ProductsService) {
    var vm = this;

    vm.products = ProductsService.query();
    vm.product = product;
    vm.error = null;
    vm.removeOne = removeOne;
    vm.addOne = addOne;
    vm.addToCart = addToCart;
    vm.openNsModal=openNsModal;
    vm.qty=1;

    vm.designs=[
        {
            _id:1,
            name:'Fingerprint Stone',
            subcategory:'fingerprint',
            image:'/modules/core/client/img/assets/designs/fingerprint/fingerprintStoneDemo.jpg'
        },
         {
            _id:2,
            name:'Fingerprint Maroon',
            subcategory:'fingerprint',
            image:'/modules/core/client/img/assets/designs/fingerprint/fingerprintMaroonDemo.jpg'
        },
         {
            _id:3,
            name:'Fingerprint Orange',
            subcategory:'fingerprint',
            image:'/modules/core/client/img/assets/designs/fingerprint/fingerprintOrangeDemo.jpg'
        },
         {
            _id:4,
            name:'Fingerprint Grey',
            subcategory:'fingerprint',
            image:'/modules/core/client/img/assets/designs/fingerprint/fingerprintGreyDemo.jpg'
        },
         {
            _id:5,
            name:'Fingerprint White',
            subcategory:'fingerprint',
            image:'/modules/core/client/img/assets/designs/fingerprint/fingerprintWhiteDemo.jpg'
        },
        
        {
            _id:6,
            name:'Alligator Black',
            subcategory:'alligator',
            image:'/modules/core/client/img/assets/designs/alligator/alligatorBlackDemo.jpg'
        },
         {
            _id:7,
            name:'Alligator Brown',
            subcategory:'alligator',
            image:'/modules/core/client/img/assets/designs/alligator/alligatorBrownDemo.jpg'
        },
         {
            _id:8,
            name:'Alligator Olive',
            subcategory:'alligator',
            image:'/modules/core/client/img/assets/designs/alligator/alligatorOliveDemo.jpg'
        },
         {
            _id:9,
            name:'Alligator Grey',
            subcategory:'alligator',
            image:'/modules/core/client/img/assets/designs/alligator/alligatorGreyDemo.jpg'
        },
         {
            _id:10,
            name:'Alligator White',
            subcategory:'alligator',
            image:'/modules/core/client/img/assets/designs/alligator/alligatorWhiteDemo.jpg'
        },
         {
            _id:11,
            name:'Yatching Color',
            subcategory:'yatching',
            image:'/modules/core/client/img/assets/designs/yatching/yatch.jpg'
        },
         {
            _id:12,
            name:'Yatching B&W',
            subcategory:'yatching',
            image:'/modules/core/client/img/assets/designs/yatching/yatchbw.jpg'
        },
    ];

    vm.productsSlider=[
        {
            _id:1,
            name:'name1',
            subcategory:'alligator',
            img:'/modules/core/client/img/assets/alligator-rolls.jpg'
        },
        {
            _id:2,
            name:'name2',
            subcategory:'yatching',
             img:'/modules/core/client/img/assets/yatching-rolls.jpg'
        },
        {
            _id:3,
            name:'name3',
           subcategory:'fingerprint',
            img:'/modules/core/client/img/assets/fingerprint-rolls.jpg'
        },
    ];

    // Remove existing Product
    function addOne() {
      vm.qty=vm.qty+1;
    }
    // Remove existing Product
    function removeOne() {
      vm.qty=vm.qty-1;
    }

    function addToCart(item,qty) {
      ShoppingCartService.addItem(item,qty)
        .then(function(res){
            vm.qty=0;
        })
        .catch(function(err){
            this.errors.other = err.message;
        });
    }

    function openNsModal(item) {
            
            vm.selected = item;
            
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/modules/products/client/views/product-modal.client.view.html',
                //controller: 'ModalInstanceCtrl',
                size: 'md',
                controllerAs:'vm',
                controller:'ProductModalController',
                resolve: {
                    selectedItem: function () {
                      return item;
                    }
                }
            });

            modalInstance.result.then(function () {
               
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
            // if (confirm('Are you sure you want to delete?')) {
            //     //vm.removeFromCart();
            // }else{
            //     return;
            // }
    }
  }
})();
