'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    async = require('async'),
    Product = mongoose.model('Product'),
    //User = mongoose.model('User'),
    errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
    _ = require('lodash');

/**
 * Create a Product
 */
exports.create = function(req, res) {
    var product = new Product(req.body);
    product.user = req.user;

    product.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(product);
        }
    });
};

/**
 * Show the current Product
 */
exports.read = function(req, res) {
    // convert mongoose document to JSON
    var product = req.product ? req.product.toJSON() : {};

    // Add a custom field to the Article, for determining if the current User is the "owner".
    // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
    product.isCurrentUserOwner = req.user && product.user && product.user._id.toString() === req.user._id.toString() ? true : false;

    res.jsonp(product);
};






/**
 * 
 */
exports.toggleIsWish = function(req, res) {

    var toggle = req.query.toggle;
    var user = req.user;

    async.waterfall([
        function(callback) {
            //remove
            if(toggle!==undefined){
                for (var i = user.wishlist.length - 1; i >= 0; i--) {
                    var wish_prod = user.wishlist[i];
                    //console.log(wish_prod.equals(req.product._id));
                    if(wish_prod.equals(req.product._id)){
                        user.wishlist.splice(i,1);
                    }
                }
            }else{
                //add
                 user.wishlist.push(req.product._id);
            }    
            callback(null, { 'wishlist-user-updated':true });
        },
        function(arg1, callback) {
            // save user 
            user.save(function(err) {
                if (err) {
                     callback(err);
                } else {
                    callback(null, { 'user-saved':true });
                }
            });
        }, 
    ], function (err, result) {
        if (err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            req.user = user;
            res.jsonp(user);
        }
    });
    
};

exports.toggleIsWatch = function(req, res) {

    var toggle = req.query.toggle;
    var user = req.user;

    async.waterfall([
        function(callback) {
            //remove
            if(toggle!==undefined){
                for (var i = user.watchlist.length - 1; i >= 0; i--) {
                    var watch_prod = user.watchlist[i];
                    //console.log(wish_prod.equals(req.product._id));
                    if(watch_prod.equals(req.product._id)){
                        user.watchlist.splice(i,1);
                    }
                }
            }else{
                //add
                 user.watchlist.push(req.product._id);
            }    
            callback(null, { 'watchlist-user-updated':true });
        },
        function(arg1, callback) {
            // save user 
            user.save(function(err) {
                if (err) {
                     callback(err);
                } else {
                    req.user = user;
                    callback(null, { 'user-saved':true });
                }
            });
        }, 
    ], function (err, result) {
        if (err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            res.jsonp(user);
        }
    });
    
};



exports.listByIds=function(req,res){

    var ids = req.params.ids;
    var query = {
        _id: { $in: ids.split(',') }
    };

  Product.find(query)
    .lean()
    .exec(function (err, result) {
    if (err) {
        res.status(400).send(err);
      } else {
        res.json(result);
      }
  });
};



/**
 * Update a Product
 */
exports.update = function(req, res) {
    var product = req.product ;

    product = _.extend(product , req.body);

    product.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(product);
        }
    });
};

/**
 * Delete an Product
 */
exports.delete = function(req, res) {
    var product = req.product ;

    product.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(product);
        }
    });
};

/**
 * List of Products
 */
exports.list = function(req, res) { 
    Product.find().sort('-created').populate('user', 'displayName').exec(function(err, products) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(products);
        }
    });
};

/**
 * Product middleware
 */
exports.productByID = function(req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).send({
            message: 'Product is invalid'
        });
    }

    Product.findById(id).populate('user', 'displayName').exec(function (err, product) {
        if (err) {
            return next(err);
        } else if (!product) {
            return res.status(404).send({
                message: 'No Product with that identifier has been found'
            });
        }
        req.product = product;
        next();
    });
};
