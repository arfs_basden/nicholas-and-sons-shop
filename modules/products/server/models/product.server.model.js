'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Product Schema
 */
var ProductSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Product name',
    trim: true
  },
  description: {
    type: String,
    default: '',
    trim: true
  },
  info:  {
    type: String,
    default: '',
    trim: true
  },
  active: Boolean,
  category: {
    type: String,
    default: '',
    trim: true
  },//MEN,WOMEN
  subcategory:String,//yachting,Signature,Alligator,Rorsarch
  price:{},
  stock:Number,
  image: {
    type: String,
    default: ''
    
  },
  
  created: {
    type: Date,
    default: Date.now
  },
  reviews:[{
    type: Schema.ObjectId,
    ref: 'Review'
  }]
});

mongoose.model('Product', ProductSchema);
