(function () {
  'use strict';

  // Orders controller
  angular
    .module('checkout')
    .controller('CheckoutController', CheckoutController);

  CheckoutController.$inject = ['$scope', '$state', 'Authentication','ShoppingCartService','CheckoutService','$rootScope'];

  function CheckoutController ($scope, $state, Authentication, ShoppingCartService,CheckoutService,$rootScope) {
    
    var vm = this;

    vm.authentication = Authentication;
    vm.getUserCart = getUserCart;
    vm.error = null;
    vm.cart=null;
    vm.form = {};
    vm.checkStepStatus=checkStepStatus;
    vm.toggleComfirmAddress=toggleComfirmAddress;
    vm.totalPrice = 0;
    vm.totalProducts = 0;
    vm.isConfirmed=null;
    vm.makePayment=makePayment;
    vm.order = {};
    vm.processingPayment=false;
    
    if (Authentication.user !== undefined && typeof Authentication.user !== 'object') {
        $state.go('authentication.signin');
    }else{
        if (Authentication.user.address.length===0){
            $state.go('settings.address');
        }
        if (vm.isConfirmed){
            $state.go('checkout.step2');
        }
    }

    function getUserCart(){
        ShoppingCartService.getShoppingCart()
            .then(function(res){
                vm.cart=res;
                vm.totalPrice=res.totalPrice;
                vm.totalProducts=res.totalProducts;
            })
            .catch(function(err){
                console.log(err);
            });
    }
    vm.getUserCart();

    function checkStepStatus(){
        CheckoutService.checkStepStatus()
            .then(function(res){
               vm.isConfirmed=res.step1; 
            })
            .catch(function(err){
                console.log(err);
            });
    }
    vm.checkStepStatus();

    function getOrderCart(id){
        ShoppingCartService.getShoppingCartById(id)
            .then(function(res){
                vm.cart=res;
                vm.totalPrice=res.totalPrice;
                vm.totalProducts=res.totalProducts;
            })
            .catch(function(err){
                console.log(err);
            });
    }

    $scope.handleStripe=function(status, response){
        if(response.error) {
          // there was an error. Fix it.
        } else {
            // got stripe token, now charge it or smt
            var stripeData={
                token:response.id,
                client_ip: response.client_ip,
            };
             vm.makePayment(stripeData);
        }
      };


    function makePayment(stripeData){
        CheckoutService.placeOrder(stripeData)
            .then(function(res){
                vm.isPayed=true;
                vm.order=res.order;
                vm.authentication.user=res.user;
                //update ns-cart directive
                $rootScope.$emit('$sync-cart-auth', true);
                //vm.bsLoadingOverlayService.stop();
                $state.go('settings.viewOrder',{ orderId:res.order._id });
            })
            .catch(function(err){
                console.log(err);
            });
    } 

    function toggleUpdateAddress(){
      vm.updateAddress=!vm.updateAddress;
    } 

    function toggleComfirmAddress(){
      vm.isConfirmed=!vm.isConfirmed;
      CheckoutService.checkStep(vm.isConfirmed)
            .then(function(res){
               vm.isConfirmed=res.step1; 
            })
            .catch(function(err){
                console.log(err);
            });
    }  
  }
})();
