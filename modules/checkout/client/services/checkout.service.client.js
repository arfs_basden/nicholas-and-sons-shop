
(function () {
    'use strict';
    
    CheckoutService.$inject = ['$location', '$http', '$cookies','$rootScope', '$q', 'Authentication','$window'];
    function CheckoutService($location, $http, $cookies, $rootScope, $q, Authentication, $window) {  
        var checkoutService = {
            checkStep: function(isConfirmed){
                var deferred =$q.defer();
                var ns_step = {
                    user: Authentication.user._id,
                    step1:isConfirmed,
                };
                $cookies.remove('ns-chek');
                $cookies.putObject('ns-chek', ns_step);
                deferred.resolve({ step1:isConfirmed });
                return deferred.promise;
            },
                    
            checkStepStatus: function(){        
                var deferred = $q.defer();
                if($cookies.getObject('ns-chek')!==undefined){
                    deferred.resolve({ step1:$cookies.getObject('ns-chek').step1 });
                        return deferred.promise;
                }else{
                    deferred.resolve({ step1:false });
                    return deferred.promise;
                }
            },
            placeOrder: function(data){        
                return $http.post('/api/orders',data)
                    .then(function(res){
                        $cookies.remove('ns-chek');
                            return res.data;
                    })
                    .catch(function(err) {
                            return $q.reject(err.data);
                    });
            }
        };
        return checkoutService;
    } 
    angular.module('checkout')
    .factory('CheckoutService', CheckoutService);
    
})();
