
'use strict';

angular.module('checkout')
	.directive('shoppingBagResume', function () {
		return {
			scope:{
				cart:'=',
				isBagCollapsed:'=?',
				totalProducts:'=',
				totalPrice:'='
			},
			templateUrl: 'modules/checkout/client/directives/shopping-bag-resume/shopping-bag-resume.html',
			restrict: 'E',
		};
	});
