(function () {
  'use strict';

  angular
    .module('orders')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {

    $stateProvider
      
      .state('checkout', {
        url: '/checkout',
        abstract: true,
        controller: 'CheckoutController',
        controllerAs: 'vm',
        templateUrl: 'modules/checkout/client/views/checkout.client.view.html',
      })
      .state('checkout.step1', {
        url: '/step1',
        templateUrl: 'modules/checkout/client/views/checkout-step1.client.view.html',
        title: 'Step 1 : Confirm your delivery address'
      })
      .state('checkout.step2', {
        url: '/step2',
        templateUrl: 'modules/checkout/client/views/checkout-step2.client.view.html',
        title: 'Step 2 : Confirm your payment details'
      })
      .state('checkout.step3', {
        url: '/step3/:orderId',
        templateUrl: 'modules/checkout/client/views/checkout-step3.client.view.html',
        title: ''
      });

      // .state('settings.viewOrder', {
      //   url: '/orders/:orderId',
      //   templateUrl: 'modules/orders/client/views/view-order.client.view.html',
      //   controller: 'OrdersController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     orderResolve: getOrder
      //   },
      //   data:{
      //     pageTitle: 'Order {{ orderResolve.name }}'
      //   }
      // });
  }

  // getOrder.$inject = ['$stateParams', 'OrdersService'];

  // function getOrder($stateParams, OrdersService) {
  //   return OrdersService.get({
  //     orderId: $stateParams.orderId
  //   }).$promise;
  // }

  // newOrder.$inject = ['OrdersService'];

  // function newOrder(OrdersService) {
  //   return new OrdersService();
  // }
})();

