'use strict';

// Setting up route
angular.module('users.admin.routes').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      // .state('admin', {
      //   abstract: true,
      //   url: '/admin',
      //   templateUrl: 'modules/users/client/views/admin/admin.client.view.html',
      //   data: {
      //     roles: ['admin']
      //   }
      // })

      .state('admin.dashboard', {
        url: '/dashboard',
        templateUrl: 'modules/users/client/views/admin/admin.dashboard.client.view.html',
        controller: 'AdminDashboardController',
        controllerAs: 'vm',
        title:'Dashboard'
      })

      .state('admin.users', {
        url: '/users',
        templateUrl: 'modules/users/client/views/admin/list-users.client.view.html',
        controller: 'UserListController',
        title:'Users'
      })
      .state('admin.user', {
        url: '/users/:userId',
        templateUrl: 'modules/users/client/views/admin/view-user.client.view.html',
        controller: 'UserController',
        title:'User Details',
        resolve: {
          userResolve: ['$stateParams', 'Admin', function ($stateParams, Admin) {
            return Admin.get({
              userId: $stateParams.userId
            });
          }]
        }
      })
      .state('admin.user-edit', {
        url: '/users/:userId/edit',
        templateUrl: 'modules/users/client/views/admin/edit-user.client.view.html',
        controller: 'UserController',
        title:'Edit User',
        resolve: {
          userResolve: ['$stateParams', 'Admin', function ($stateParams, Admin) {
            return Admin.get({
              userId: $stateParams.userId
            });
          }]
        }
      })

      .state('admin.products', {
        url: '/products',
        templateUrl: 'modules/users/client/views/admin/admin.products.client.view.html',
        controller: 'AdminProductsListController',
        controllerAs: 'vm',
        title:'Products',
        data: {
          pageTitle: 'Products List'
        }
      })
      .state('admin.product', {
        url: '/products/:productId',
        templateUrl: 'modules/users/client/views/admin/admin.product.client.view.html',
        controller: 'AdminProductController',
        controllerAs: 'vm',
        title:'Edit Product',
        resolve: {
          prodResolve: ['$stateParams', 'ProductsService', function ($stateParams, ProductsService) {
            return ProductsService.get({
              productId: $stateParams.productId
            });
          }]
        }
      })

      ;
  }
]);
