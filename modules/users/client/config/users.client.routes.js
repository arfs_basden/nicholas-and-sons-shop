'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider
      .state('settings', {
        abstract: true,
        url: '/settings',
        templateUrl: 'modules/users/client/views/settings/settings.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('settings.dashboard', {
        url: '/dashboard',
        templateUrl: 'modules/users/client/views/settings/settings.dashboard.client.view.html',
        title:'Dashboard'
      })
      .state('settings.profile', {
        url: '/profile',
        templateUrl: 'modules/users/client/views/settings/settings.edit-profile.client.view.html',
        title:'Edit Profile'
      })
      .state('settings.password', {
        url: '/password',
        templateUrl: 'modules/users/client/views/settings/settings.change-password.client.view.html',
        title:'Change Password'
      })
      .state('settings.wishlist', {
        url: '/wishlist',
        templateUrl: 'modules/users/client/views/settings/settings.wishlist.client.view.html',
        title:'Wish List'
      })
      .state('settings.watchlist', {
        url: '/watchlist',
        templateUrl: 'modules/users/client/views/settings/settings.watchlist.client.view.html',
        title:'Watch List'
      })
      .state('settings.address', {
        url: '/address',
        templateUrl: 'modules/users/client/views/settings/settings.address.client.view.html',
        title:'Delivery Address',
      })
      .state('settings.picture', {
        url: '/picture',
        templateUrl: 'modules/users/client/views/settings/settings.change-profile-picture.client.view.html'
      })
      .state('settings.orders', {
        url: '/orders',
        templateUrl: 'modules/users/client/views/settings/settings.orders.client.view.html',
        controller: 'OrdersUserListController',
        controllerAs: 'vm',
        title: 'Orders'
      })
      .state('settings.viewOrder', {
        url: '/orders/:orderId',
        templateUrl: 'modules/users/client/views/settings/settings.order.client.view.html',
        controller: 'OrdersUserController',
        controllerAs: 'vm',
        resolve: {
          orderResolve: getOrder
        },
        title:'Order Details'
        
      })
      
      .state('authentication', {
        abstract: true,
        url: '/authentication',
        templateUrl: 'modules/users/client/views/authentication/authentication.client.view.html'
      })
      .state('authentication.signup', {
        url: '/signup',
        templateUrl: 'modules/users/client/views/authentication/signup.client.view.html'
      })
      .state('authentication.signin', {
        url: '/signin?err',
        templateUrl: 'modules/users/client/views/authentication/signin.client.view.html'
      })
      .state('password', {
        abstract: true,
        url: '/password',
        template: '<ui-view/>'
      })
      .state('password.forgot', {
        url: '/forgot',
        templateUrl: 'modules/users/client/views/password/forgot-password.client.view.html'
      })
      .state('password.reset', {
        abstract: true,
        url: '/reset',
        template: '<ui-view/>'
      })
      .state('password.reset.invalid', {
        url: '/invalid',
        templateUrl: 'modules/users/client/views/password/reset-password-invalid.client.view.html'
      })
      .state('password.reset.success', {
        url: '/success',
        templateUrl: 'modules/users/client/views/password/reset-password-success.client.view.html'
      })
      .state('password.reset.form', {
        url: '/:token',
        templateUrl: 'modules/users/client/views/password/reset-password.client.view.html'
      });
  }
]);

getOrder.$inject = ['$stateParams', 'OrdersService'];

  function getOrder($stateParams, OrdersService) {
    return OrdersService.get({
      orderId: $stateParams.orderId
    }).$promise;
  }

  newOrder.$inject = ['OrdersService'];

  function newOrder(OrdersService) {
    return new OrdersService();
  }




