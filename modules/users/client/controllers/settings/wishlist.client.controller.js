'use strict';
angular.module('users')
	.controller('WishListController', ['$scope', 'Authentication','WishService','$http',
  		function ($scope, Authentication, WishService, $http) {
			$scope.user = Authentication.user;
			$scope.getWishList=function(){
				var ids=$scope.user.wishlist;
				$http.get('/api/products/list/'+ ids.join(','))
					.then(function(res2){
						$scope.items= res2.data;
					})
					.catch(function(err){
						console.log(err);
					});
			};
			$scope.getWishList();

			$scope.removeWishList = function (id){
				WishService.removeItem(id)
					.then(function(res){
						//console.log(res);
						$scope.user=res;
						$scope.getWishList();
					})
					.catch(function(err){
						console.log(err);
					});
			};

  		}
	]);
