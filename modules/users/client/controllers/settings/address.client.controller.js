'use strict';

angular.module('users')
    .controller('AddressController', ['$scope', '$http', 'Authentication', '$state','toastr','ShoppingCartService',
        function ($scope, $http, Authentication,$state,toastr,ShoppingCartService) {
            $scope.user = Authentication.user;

            $scope.getUserCart=function(){
                ShoppingCartService.getShoppingCart()
                    .then(function(res){
                            $scope.totalPrice=res.totalPrice;
                            $scope.totalProducts=res.totalProducts;
                    })
                    .catch(function(err){
                        console.log(err);
                    });
            };
            $scope.getUserCart();
            $scope.address = $scope.user.address[0];// || $scope.mockaddress;
            $scope.noAddress=$scope.user.address.length===0;
            $scope.updateAddress=false;

            $scope.toggleUpdateAddress=function(){
                $scope.updateAddress=!$scope.updateAddress;
            };

        // Change user password
            $scope.saveAddress = function (isValid) {
                $scope.success = $scope.error = null;
                $scope.submitted=true;

                if (!isValid) {
                     $scope.$broadcast('show-errors-check-validity', 'addressForm');
                     return false;
                }
                
                var url='/api/users/address';
                
                if ($scope.updateAddress && !$scope.noAddress) {
                    url=url+'?update=true';
                }

                var payload=$scope.address;

                $http.post(url, payload).success(function (response) {

                    $scope.user.address=response.address;
                    $scope.$broadcast('show-errors-reset', 'addressForm');
                    $scope.success = true;
                    $scope.updateAddress=false;
                    $scope.submitted=false;
                    toastr.success('Your address has been updated correctly.', 'DELIVERY ADDRESS UPDATED!');
                    //window.location.href='/settings/address';
                }).error(function (response) {
                        $scope.error = response.message;
                });
            };
            $scope.removeAddress = function () {
                var url='/api/users/address?remove=true';
                var payload=$scope.address;
                $http.post(url, payload)
                    .success(function (response) {
                    // If successful show success message and clear form
                    //console.log(response);
                    $scope.user.address=response.address;
                    toastr.success('Your address has been removed correctly.', 'DELIVERY ADDRESS REMOVED!');
                    $scope.noAddress=$scope.user.address.length===0;
                })
                .error(function(response){
                    $scope.error = response.message;
                });
            };
        }
]);

