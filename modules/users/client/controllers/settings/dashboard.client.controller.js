'use strict';
angular.module('users')
	.controller('DashboardController', ['$scope', 'Authentication','$http','ShoppingCartService',
  		function ($scope, Authentication,$http,ShoppingCartService) {
			$scope.user = Authentication.user;

			$scope.getUserCart=function(){
                ShoppingCartService.getShoppingCart()
                    .then(function(res){
                        $scope.totalPrice=res.totalPrice;
                        $scope.totalProducts=res.totalProducts;
                    })
                    .catch(function(err){
                        console.log(err);
                    });
            };
            $scope.getUserCart();


  		}
	]);

