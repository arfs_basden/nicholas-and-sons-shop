  (function () {

  'use strict';

  angular
    .module('users')
    .controller('OrdersUserListController', OrdersUserListController);

  OrdersUserListController.$inject = ['OrdersService'];

	function OrdersUserListController(OrdersService) {
		var vm = this;
    	vm.orders = OrdersService.query();
	}
})();


