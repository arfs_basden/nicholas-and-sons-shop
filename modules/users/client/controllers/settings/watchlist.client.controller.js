'use strict';
angular.module('users')
	.controller('WatchListController', ['$scope', 'Authentication','WatchListService','$http',
  		function ($scope, Authentication, WatchListService, $http) {
    		$scope.user = Authentication.user;

    		$scope.getWatchList=function(){

    			var ids=$scope.user.watchlist;
    			$http.get('/api/products/list/'+ ids.join(','))
					.then(function(res2){
						$scope.items= res2.data;
					})
					.catch(function(err){
						console.log(err);
					});
    		};
			$scope.getWatchList();

			$scope.removeWatchList = function (id){
		      	WatchListService.removeItem(id)
					.then(function(res){
						//console.log(res);
						$scope.user=res;
						$scope.getWatchList();
					})
					.catch(function(err){
						console.log(err);
					});
		};
  		}
	]);

