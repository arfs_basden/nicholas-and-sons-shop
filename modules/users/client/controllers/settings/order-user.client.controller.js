(function () {
  'use strict';

  // Orders controller
  angular
    .module('orders')
    .controller('OrdersUserController', OrdersUserController);

  OrdersUserController.$inject = ['$scope', '$state', 'Authentication', 'orderResolve','ShoppingCartService'];

  function OrdersUserController ($scope, $state, Authentication, order,ShoppingCartService) {
    var vm = this;

    vm.authentication = Authentication;
    vm.order = order;
    vm.cart = null;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.getOrderCart=getOrderCart;
 	vm.totalPrice=0;
    vm.totalProducts=0;


    function getOrderCart(){
    	
        ShoppingCartService.getShoppingCartById(order.cart)
            .then(function(res){
            	vm.cart=res;
                vm.totalPrice=res.totalPrice;
                vm.totalProducts=res.totalProducts;
            })
            .catch(function(err){
                console.log(err);
            });
    }
    vm.getOrderCart();
    
    // Remove existing Order
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.order.$remove($state.go('orders.list'));
      }
    }

    // Save Order
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.orderForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.order._id) {
        vm.order.$update(successCallback, errorCallback);
      } else {
        vm.order.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('orders.view', {
          orderId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
})();

