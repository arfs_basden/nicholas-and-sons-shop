  (function () {

  'use strict';

  angular
    .module('users.admin')
    .controller('AdminProductController', AdminProductController);

  AdminProductController.$inject = ['$scope','ProductsService','prodResolve','toastr'];

	function AdminProductController($scope,ProductsService, prodResolve,toastr) {
		var vm = this;
		vm.product = prodResolve;
		// Update existing Article
		$scope.update = function (isValid) {
			$scope.error = null;

			if (!isValid) {
			$scope.$broadcast('show-errors-check-validity', 'articleForm');

			return false;
			}
			var product = vm.product;
			product.$update(function () {
				toastr.success('Item updated', ' UPDATED!');
			//$location.path('product/' + article._id);
			}, function (errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

	}
})();

