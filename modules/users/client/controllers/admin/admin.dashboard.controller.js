(function () {
  'use strict';
 	angular
		.module('users.admin')
		.controller('AdminDashboardController', AdminDashboardController);
	AdminDashboardController.$inject = ['ProductsService','$rootScope','ShoppingCartService','Authentication','Admin','WatchListService'];
	function AdminDashboardController(ProductsService,$rootScope, ShoppingCartService,Authentication,Admin,WatchListService) {
		var vm = this;
		vm.users = Admin.query();
		vm.products = ProductsService.query();
	}
})();

