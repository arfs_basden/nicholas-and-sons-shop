'use strict';

angular.module('users.admin').controller('UserController', ['$scope', '$state', 'Authentication', 'userResolve',
  function ($scope, $state, Authentication, userResolve) {
    $scope.authentication = Authentication;
    $scope.user = userResolve;
    $scope.isCollapsed=true;

    $scope.isCollapsed2=true;
    $scope.isCollapsed3=true;
    $scope.isCollapsed4=true;
    $scope.isCollapsed5=true;

    $scope.isCollapsedEdit=false;
    //$scope.address=userResolve.address[0];

    $scope.remove = function (user) {
      if (confirm('Are you sure you want to delete this user?')) {
        if (user) {
          user.$remove();

          $scope.users.splice($scope.users.indexOf(user), 1);
        } else {
          $scope.user.$remove(function () {
            $state.go('admin.users');
          });
        }
      }
    };

  //  db.users.update(
  //    { username: "alvaro.fernandez" },
  //    { $push: { roles  : "admin" } }
  //  )


    $scope.update = function (isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      var user = $scope.user;

      user.$update(function () {
        $state.go('admin.user', {
          userId: user._id
        });
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
  }
]);
