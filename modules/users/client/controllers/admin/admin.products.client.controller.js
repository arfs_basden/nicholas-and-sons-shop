  (function () {

  'use strict';

  angular
    .module('users.admin')
    .controller('AdminProductsListController', AdminProductsListController);

  AdminProductsListController.$inject = ['ProductsService','$filter'];

	function AdminProductsListController(ProductsService, $filter) {
		var vm = this;

		vm.pagedItems = [];
		vm.filteredItems=[];
		//vm.search='';
		vm.itemsPerPage = 0;
		vm.currentPage = 0;
		vm.filterLength=0;
		vm.buildPager = buildPager;
		vm.pageChanged = pageChanged;
		vm.figureOutItemsToDisplay = figureOutItemsToDisplay;
		vm.products =[];

		vm.products = ProductsService.query();

		//vm.buildPager();

		function buildPager() {
			
			vm.pagedItems = [];
			vm.itemsPerPage = 15;
			vm.currentPage = 1;
			vm.figureOutItemsToDisplay();
		}

		 function figureOutItemsToDisplay(){
			if(vm.search!==''){
				vm.filteredItems = $filter('filter')(vm.products, {
					$: vm.search
				});
			}else{
				vm.filteredItems = vm.products;
			}
			
			vm.filterLength = vm.filteredItems.length;
			var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
			var end = begin + vm.itemsPerPage;
			vm.pagedItems = vm.filteredItems.slice(begin, end);
		}

		function pageChanged() {
			vm.figureOutItemsToDisplay();
		}

	}
})();
