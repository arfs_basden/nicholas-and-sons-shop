'use strict';

angular.module('users')
  .controller('ServiceBannerCtrl', function ($scope,$location) {

  	$scope.logos=[
  		
  		{
  			img:'arla.jpg',
  			link:'http://www.arla.com/'
  		},
  		{
  			img:'naea.jpg',
  			link:'http://www.naea.co.uk/'
  		},
  		{
  			img:'ombudsman.jpg',
  			link:'https://www.tpos.co.uk/'
  		},
  		{
  			img:'dps.jpg',
  			link:'https://www.depositprotection.com/'
  		},
  	];
    $scope.host = $location.host();

    
  })
  .directive('servicesBanner', function () {
    return {
      replace:true,
      templateUrl: 'modules/users/client/directives/services-banner/services-banner.html',
      restrict: 'E',
      controller: 'ServiceBannerCtrl'
    };
  });