
'use strict';

// Users directive used to force lowercase input
angular.module('users')
	.directive('userAddress', function () {
		return {
			scope:{
				address:'=',
				isAddressCollapsed:'=?',
				isAddressConfirmed:'='
			},
			templateUrl: 'modules/users/client/directives/address/address.html',
			restrict: 'E',
			//controller: 'nsCartController'
		};
	});