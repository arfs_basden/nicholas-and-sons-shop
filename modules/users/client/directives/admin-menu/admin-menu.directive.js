'use strict';

// Users directive used to force lowercase input
angular.module('users')
	.directive('adminMenu', function () {
		return {
			
			templateUrl: 'modules/users/client/directives/admin-menu/admin-menu.html',
			restrict: 'E',
			//controller: 'nsCartController'
		};
	});