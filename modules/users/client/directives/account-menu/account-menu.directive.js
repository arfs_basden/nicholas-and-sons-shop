'use strict';

// Users directive used to force lowercase input
angular.module('users')
	.directive('accountMenu', function () {
		return {
			templateUrl: 'modules/users/client/directives/account-menu/account-menu.html',
			restrict: 'E',
			//controller: 'nsCartController'
		};
	});